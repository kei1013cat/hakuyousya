<?php
date_default_timezone_set('Asia/Tokyo');
$update  = strtotime("2019-01-01 00:00");
$nowdate = strtotime(date('Y-m-d H:i'));
if(isset($_GET['date'])){
    $nowdate = strtotime($_GET['date']);
}


$str_list = array(
	array(
		'store_nm'  => 'アピアサービス店',
		'address' => '札幌市中央区北5条西4丁目札幌駅南口アピア地下街',
		'telephone' =>'011-209-3424',
		'open_time' => '8:00～20:00 　日・祝10:00～19:00',
		'holiday' => '不定期',
		'other' => '年末・年始等営業時間変更有り',
		'image' => 'shop_apia.jpg',
		'map' => 'https://www.google.co.jp/maps/place/%E7%99%BD%E6%B4%8B%E8%88%8E/@43.0667714,141.3491708,18.75z/data=!3m1!5s0x5f0b2975161e0ceb:0x4ceb5b0554ce4b70!4m12!1m6!3m5!1s0x5f0b297569985de9:0x969f0a9c36d0b11!2z55m95rSL6IiO!8m2!3d43.0667881!4d141.3496305!3m4!1s0x5f0b297569985de9:0x969f0a9c36d0b11!8m2!3d43.0667881!4d141.3496305',
		'morning_open' => true,
		'night_open' => true,
		'Weekends_open' => false,
		'quick_service' => true,
		'delivery_service' => false,
	),
	array(
		'store_nm'  => 'さっぽろ東急サービス店',
		'address' => '札幌市中央区北4条西2丁目 さっぽろ東急百貨店内B1F',
		'telephone' =>'011-212-2442',
		'open_time' => '10：00～20：00',
		'holiday' => 'さっぽろ東急百貨店に準ずる',
		'other' => '年末・年始等時間変更有り',
		'image' => 'shop_toukyu.jpg?v=201808',
		'map' => 'https://www.google.co.jp/maps/place/%E7%99%BD%E6%B4%8B%E8%88%8E/@43.0663303,141.3521359,18z/data=!3m1!5s0x5f0b2975adce922b:0x4ceb5b05e28795c8!4m5!3m4!1s0x5f0b2975ae0a0047:0x55ce7005dc4d9f0f!8m2!3d43.0662088!4d141.3524455',
		'morning_open' => false,
		'night_open' => true,
		'Weekends_open' => true,
		'quick_service' => false,
		'delivery_service' => false,
	),
	array(
		'store_nm'  => '時計台前サービス店',
		'address' => '札幌市中央区北1条西3丁目3-14',
		'telephone' =>'011-241-0057',
		'open_time' => '8:00～19:00 　　土9:00～18:00 　　日・祝9:00～18:00',
		'holiday' => '不定休',
		'other' => '年末・年始等営業時間変更有り',
		'image' => 'shop_tokeidai.jpg',
		'map' => 'https://www.google.co.jp/maps/place/%E6%9C%AD%E5%B9%8C%E7%99%BD%E6%B4%8B%E8%88%8E%EF%BC%88%E6%A0%AA%EF%BC%89+%E6%99%82%E8%A8%88%E5%8F%B0%E5%89%8D%E3%82%B5%E3%83%BC%E3%83%93%E3%82%B9%E5%BA%97/@43.0625854,141.3517397,17.5z/data=!4m12!1m6!3m5!1s0x5f0b299dbc0cdf3d:0xfcaf551cac28c0fa!2z5pyt5bmM55m95rSL6IiO77yI5qCq77yJIOaZguioiOWPsOWJjeOCteODvOODk-OCueW6lw!8m2!3d43.062454!4d141.352176!3m4!1s0x5f0b299dbc0cdf3d:0xfcaf551cac28c0fa!8m2!3d43.062454!4d141.352176',
		'morning_open' => true,
		'night_open' => true,
		'Weekends_open' => false,
		'quick_service' => true,
		'delivery_service' => false,
	),
	array(
		'store_nm'  => '三越札幌サービス店',
		'address' => '札幌市中央区南1条西3丁目 三越B2F',
		'telephone' =>'011-222-2970',
		'open_time' => '10：00～19：30',
		'holiday' => '不定期',
		'other' => '年末・年始等時間変更有り',
		'image' => 'shop_mitsukoshi.jpg',
		'map' => 'https://www.google.co.jp/maps/place/%E7%99%BD%E6%B4%8B%E8%88%8E%EF%BC%88%E6%A0%AA%EF%BC%89+%E4%B8%89%E8%B6%8A%E6%9C%AD%E5%B9%8C%E5%BA%97/@43.0596952,141.3528979,17.87z/data=!3m1!5s0x5f0b299cca6019e7:0xcaa3eba4732c87d6!4m8!1m2!2m1!1z55m95rSL6IiO44CA5pyt5bmM5biC5Lit5aSu5Yy65Y2XMeadoeilvzPkuIHnm64g5LiJ6LaK!3m4!1s0x0:0xebcf3619def29052!8m2!3d43.0593169!4d141.3533123',
		'morning_open' => false,
		'night_open' => true,
		'Weekends_open' => false,
		'quick_service' => false,
		'delivery_service' => false,
	),
	array(
		'store_nm'  => 'マックスバリュ平岸サービス店',
		'address' => '札幌市豊平区平岸3条13丁目6-1 マックスバリュ平岸店1F',
		'telephone' =>'011-812-6566',
		'open_time' => '10:00～19:00',
		'holiday' => 'マックスバリュ平岸店に準ずる',
		'other' => '年末・年始等営業時間変更有り',
		'image' => 'shop_mv_hiragishi.jpg',
		'map' => 'https://www.google.co.jp/maps/place/%E7%99%BD%E6%B4%8B%E8%88%8E%EF%BC%88%E6%A0%AA%EF%BC%89+%E3%83%9E%E3%83%83%E3%82%AF%E3%82%B9%E3%83%90%E3%83%AA%E3%83%A5+%E5%B9%B3%E5%B2%B8%E3%82%B5%E3%83%BC%E3%83%93%E3%82%B9%E5%BA%97/@43.0269822,141.3676921,16.54z/data=!4m12!1m6!3m5!1s0x5f0b2a68493295c1:0xbf4596d4e7a8c773!2z55m95rSL6IiO77yI5qCq77yJIOODnuODg-OCr-OCueODkOODquODpSDlubPlsrjjgrXjg7zjg5Pjgrnlupc!8m2!3d43.026722!4d141.370708!3m4!1s0x5f0b2a68493295c1:0xbf4596d4e7a8c773!8m2!3d43.026722!4d141.370708',
		'morning_open' => false,
		'night_open' => true,
		'Weekends_open' => true,
		'quick_service' => false,
		'delivery_service' => false,
	),
	array(
		'store_nm'  => '琴似サービス店',
		'address' => '札幌市西区山の手6条1丁目3-34',
		'telephone' =>'011-631-2308',
		'open_time' => '9：00～19：00　　日・祝9：00～18：00　　年末・年始等営業時間変更有り',
		'holiday' => '不定期',
		'other' => '年末・年始・夏期休業有り',
		'image' => 'shop_kotoni.jpg',
		'map' => 'https://www.google.co.jp/maps/place/%E3%80%92063-0006+%E5%8C%97%E6%B5%B7%E9%81%93%E6%9C%AD%E5%B9%8C%E5%B8%82%E8%A5%BF%E5%8C%BA%E5%B1%B1%E3%81%AE%E6%89%8B%EF%BC%96%E6%9D%A1%EF%BC%91%E4%B8%81%E7%9B%AE%EF%BC%93+%E7%99%BD%E6%B4%8B%E8%88%8E%E7%90%B4%E4%BC%BC%E3%82%B5%E3%83%BC%E3%83%93%E3%82%B9%E5%BA%97/@43.0760392,141.2948542,17z/data=!4m5!3m4!1s0x5f0b2837dc69c29d:0x97f3f9f8f094e58b!8m2!3d43.0758662!4d141.2941002',
		'morning_open' => true,
		'night_open' => true,
		'Weekends_open' => false,
		'quick_service' => true,
		'delivery_service' => false,
	),
	array(
		'store_nm'  => '西友西町サービス店',
		'address' => '札幌市西区西町南6丁目1 西友西町店1F',
		'telephone' =>'011-664-1820',
		'open_time' => '10：00～19：00',
		'holiday' => '不定期',
		'other' => '年末・年始・夏期等営業時間変更有り',
		'image' => 'shop_seiyu.jpg',
		'map' => 'https://www.google.co.jp/maps/place/%E6%9C%AD%E5%B9%8C%E7%99%BD%E6%B4%8B%E8%88%8E%EF%BC%88%E6%A0%AA%EF%BC%89+%E8%A5%BF%E5%8F%8B%E8%A5%BF%E7%94%BA%E3%82%B5%E3%83%BC%E3%83%93%E3%82%B9%E5%BA%97/@43.0755943,141.2928869,16.5z/data=!4m8!1m2!2m1!1z55m95rSL6IiO44CA5pyt5bmM5biC6KW_5Yy66KW_55S65Y2XNuS4geebrjEg6KW_5Y-L6KW_55S65bqXMUY!3m4!1s0x0:0x55ca084f058d23ea!8m2!3d43.0781268!4d141.2887864',
		'morning_open' => false,
		'night_open' => true,
		'Weekends_open' => false,
		'quick_service' => false,
		'delivery_service' => false,
	),
	array(
		'store_nm'  => 'マックスバリュ琴似サービス店',
		'address' => '札幌市西区琴似2条4丁目 マックスバリュ琴似店内',
		'telephone' =>'011-642-9237',
		'open_time' => '10：00～19：00',
		'holiday' => 'マックスバリュ琴似店に準ずる',
		'other' => '',
		'image' => 'shop_mv_kotoni.jpg',
		'map' => 'https://www.google.co.jp/maps/place/%E6%9C%AD%E5%B9%8C%E7%99%BD%E6%B4%8B%E8%88%8E%EF%BC%88%E6%A0%AA%EF%BC%89+%E3%83%9E%E3%83%83%E3%82%AF%E3%82%B9%E3%83%90%E3%83%AA%E3%83%A5+%E7%90%B4%E4%BC%BC%E3%82%B5%E3%83%BC%E3%83%93%E3%82%B9%E5%BA%97/@43.0770269,141.3013243,17z/data=!4m12!1m6!3m5!1s0x5f0b284eb6f18db5:0x305b394ff092689a!2z5pyt5bmM55m95rSL6IiO77yI5qCq77yJIOODnuODg-OCr-OCueODkOODquODpSDnkLTkvLzjgrXjg7zjg5Pjgrnlupc!8m2!3d43.077023!4d141.303513!3m4!1s0x5f0b284eb6f18db5:0x305b394ff092689a!8m2!3d43.077023!4d141.303513',
		'morning_open' => false,
		'night_open' => true,
		'Weekends_open' => true,
		'quick_service' => false,
		'delivery_service' => false,
	),
	
	array(
		'store_nm'  => '真駒内サービス店',
		'address' => '札幌市南区真駒内泉町2丁目2-7',
		'telephone' =>'011-581-3003',
		'open_time' => '8：00～17：00 祝9：00～17：00 日曜休業',
		'holiday' => '不定休',
		'other' => '年末・年始・夏期休業有',
		'image' => 'shop_makomanai.jpg',
		'map' => 'https://www.google.co.jp/maps/place/%E3%80%92005-0015+%E5%8C%97%E6%B5%B7%E9%81%93%E6%9C%AD%E5%B9%8C%E5%B8%82%E5%8D%97%E5%8C%BA%E7%9C%9F%E9%A7%92%E5%86%85%E6%B3%89%E7%94%BA%EF%BC%92%E4%B8%81%E7%9B%AE%EF%BC%92%E2%88%92%EF%BC%97+%E3%82%AF%E3%83%AA%E3%83%BC%E3%83%8B%E3%83%B3%E3%82%B0%E7%99%BD%E6%B4%8B%E8%88%8E%E7%9C%9F%E9%A7%92%E5%86%85%E3%82%B5%E3%83%BC%E3%83%93%E3%82%B9%E5%BA%97/@42.9855042,141.3511789,17z/data=!3m1!4b1!4m5!3m4!1s0x5f0ad53e09444093:0x9932b53a658e2e30!8m2!3d42.9855008!4d141.3533557',
		'morning_open' => true,
		'night_open' => false,
		'Weekends_open' => false,
		'quick_service' => false,
		'delivery_service' => false,
	),

	array(
		'store_nm'  => '宮の森サービス店',
		'address' => '札幌市中央区北5条西28丁目2-2 第3福長ビル1F',
		'telephone' =>'011-633-4570',
		'open_time' => '9：00～19：00 土・日・祝9：00～18：00 昼休憩13：30～14：30',
		'holiday' => '不定休',
		'other' => '年末・年始・夏期休業有',
		'image' => 'shop_miyanomori.jpg',
		'map' => 'https://www.google.co.jp/maps/place/%E6%9C%AD%E5%B9%8C%E7%99%BD%E6%B4%8B%E8%88%8D%EF%BC%88%E6%A0%AA%EF%BC%89+%E5%AE%AE%E3%81%AE%E6%A3%AE%E3%82%B5%E3%83%BC%E3%83%93%E3%82%B9%E5%BA%97/@43.0618567,141.311308,17z/data=!4m12!1m6!3m5!1s0x5f0b29c7a8fb5ae1:0x117293182191c291!2z5pyt5bmM55m95rSL6IiN77yI5qCq77yJIOWuruOBruajruOCteODvOODk-OCueW6lw!8m2!3d43.0618528!4d141.3134967!3m4!1s0x5f0b29c7a8fb5ae1:0x117293182191c291!8m2!3d43.0618528!4d141.3134967',
		'morning_open' => true,
		'night_open' => true,
		'Weekends_open' => true,
		'quick_service' => false,
		'delivery_service' => false,
	),

	
	
	

);
?>
    <section class="icon_list">
      <ul class="cf">
        <li><img src="<?php bloginfo('template_url'); ?>/images/shop_icon-01.svg" alt="早朝営業 | 札幌白洋舍"><br>早朝営業<br>（9時前から営業） </li>
        <li><img src="<?php bloginfo('template_url'); ?>/images/shop_icon-02.svg" alt="夜間営業 | 札幌白洋舍"><br>夜間営業<br>（18時以降営業）</li>
        <li><img src="<?php bloginfo('template_url'); ?>/images/shop_icon-03.svg" alt="土日営業 | 札幌白洋舍"><br>土日営業<br>（一部の場合を含む）</li>
        <li><img src="<?php bloginfo('template_url'); ?>/images/shop_icon-04.svg" alt="クイックサービスあり | 札幌白洋舍"><br>クイックサービス<br>あり</li>
        <!--<li><img src="images/shop_icon-05.svg"><br>集配サービス<br>あり</li>-->
      </ul>
    </section>
    <section class="shop_list">
      <div class="wrapper">
        <ul>

		<?php foreach($str_list as $str): ?>

          <li class="cf">
            <div class="title cf">
              <h3><?php echo $str['store_nm']; ?></h3>
              <div class="icon">
              	<?php if($str['morning_open']==true): ?><img src="<?php bloginfo('template_url'); ?>/images/shop_icon-01.svg" alt="早朝営業 | 札幌白洋舍"><?php endif; ?>
              	<?php if($str['night_open']==true): ?><img src="<?php bloginfo('template_url'); ?>/images/shop_icon-02.svg" alt="夜間営業 | 札幌白洋舍"><?php endif; ?>
              	<?php if($str['Weekends_open']==true): ?><img src="<?php bloginfo('template_url'); ?>/images/shop_icon-03.svg" alt="土日営業 | 札幌白洋舍"><?php endif; ?>
              	<?php if($str['quick_service']==true): ?><img src="<?php bloginfo('template_url'); ?>/images/shop_icon-04.svg" alt="クイックサービスあり | 札幌白洋舍"><?php endif; ?>
              	<?php if($str['delivery_service']==true): ?><img src="<?php bloginfo('template_url'); ?>/images/shop_icon-05.svg"><?php endif; ?>
              </div>
              <!-- icon --></div>
            <!--title -->
            <?php if(!empty($str['image'])): ?><div class="photo"><img src="<?php bloginfo('template_url'); ?>/images/<?php echo $str['image'] ?>" alt="<?php echo $str['store_nm']; ?> | 札幌白洋舍"></div><?php endif; ?>
            <!-- photo -->
            <div class="cont">
              <dl class="cf">
                <dt>住所</dt>
                <dd><?php echo $str['address']; ?><a href="<?php echo $str['map'] ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/shop_icon_map.svg" class="icon" alt="GoogleMap | 札幌白洋舍"></a></dd>
              </dl>
              <dl class="cf">
                <dt>電話番号</dt>
                <dd><?php echo $str['telephone'] ?></dd>
              </dl>
              <dl class="cf">
                <dt>営業時間</dt>
                <dd><?php echo $str['open_time']; ?></dd>
              </dl>
              <dl class="cf">
                <dt>休日</dt>
                <dd><?php echo $str['holiday']; ?></dd>
              </dl>
              <dl class="cf">
                <dt>その他</dt>
                <dd><?php echo $str['other']; ?></dd>
              </dl>
            </div>
            <!-- cont --> 
          </li>
			
		<?php endforeach; ?>
        </ul>
      </div>
      <!-- wrapper --> 
    </section>
