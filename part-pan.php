<?php
    $parent_id = $post->post_parent; // 親ページのIDを取得
    $parent_slug = get_post($parent_id)->post_name; // 親ページのスラッグを取得
?>
<div id="pan">
	<?php if( is_single() ): ?>
		<?php if( $post->post_type =="recruit" ):?>
            <div class="wrapper"><a href="<?php bloginfo('url'); ?>">ホーム</a>&nbsp;&gt;&nbsp;<a href="<?php bloginfo('url'); ?>/recruit_list/">中途採用情報</a>&nbsp;&gt;&nbsp;募集要項</div><!-- wrapper -->
        <?php elseif( $post->post_type =="newgraduate" ):?>
            <div class="wrapper"><a href="<?php bloginfo('url'); ?>">ホーム</a>&nbsp;&gt;&nbsp;<a href="<?php bloginfo('url'); ?>/newgraduate_list/">新卒採用情報</a>&nbsp;&gt;&nbsp;募集要項</div><!-- wrapper -->
        <?php else: ?>
            <div class="wrapper"><a href="<?php bloginfo('url'); ?>">ホーム</a>&nbsp;&gt;&nbsp;<a href="<?php bloginfo('url'); ?>/news/">お知らせ</a>&nbsp;&gt;&nbsp;<?php echo get_the_title() ?></div><!-- wrapper -->
        <?php endif; ?>

	<?php  elseif ( is_archive() ): ?>
            <div class="wrapper"><a href="<?php bloginfo('url'); ?>">ホーム</a>&nbsp;&gt;&nbsp;お知らせ</div><!-- wrapper -->

	<?php elseif( $post->post_name=="recruit_form" || $parent_slug=="recruit_form" ): ?>
		<div class="wrapper">
			<a href="<?php bloginfo('url'); ?>">ホーム</a>
			&nbsp;&gt;&nbsp;<a href="<?php bloginfo('url'); ?>/recruit_list/">採用情報</a>
			&nbsp;&gt;&nbsp;応募フォーム
		</div><!-- wrapper -->

	<?php else: ?>
			<div class="wrapper"><a href="<?php bloginfo('url'); ?>">ホーム</a>&nbsp;&gt;&nbsp;<?php echo get_the_title() ?></div><!-- wrapper -->
	<?php endif; ?>
</div>
<!-- pan -->