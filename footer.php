</div>
<!-- contents -->
<?php if(is_pc()):?>

<p><a href="#" id="page-top"></a></p>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/top.js"></script>
<?php endif; ?>
<section id="location">
  <h2><a href="<?php bloginfo('url'); ?>/" ><img src="<?php bloginfo('template_url'); ?>/images/header_logo.png" alt="札幌白洋舍株式会社"></a></h2>
  <address>
  北海道札幌市西区山の手6条1丁目3-34
  </address>
  <p class="tel"><span>Tel:</span> <a href="tel:011-631-2306">011-631-2306</a></p>
</section>
<footer>
  <div class="wrapper">
    <nav>
      <ul class="cf">
        <?php if(is_pc()):?>
        <li><a href="<?php bloginfo('url'); ?>/service/" >サービス</a>
          <p><a href="<?php bloginfo('url'); ?>/service/">取扱い商品</a></p>
          <p><a href="<?php bloginfo('url'); ?>/price/">料金</a></p>
          <p><a href="<?php bloginfo('url'); ?>/service_course/">クリーニングコース</a></p>
          <p><a href="<?php bloginfo('url'); ?>/delivery/">集配サービス</a></p>
        </li>
        <li><a href="<?php bloginfo('url'); ?>/shop/" >店舗情報</a></li>
        <li><a href="<?php bloginfo('url'); ?>/company/" >会社案内</a></li>
        <li><a href="<?php bloginfo('url'); ?>/newgraduate_list/" >新卒採用情報</a><br /><br /><a href="<?php bloginfo('url'); ?>/recruit_list/" >中途採用情報</a></li>
        <li><a href="<?php bloginfo('url'); ?>/inquiry/" >お問い合わせ</a>
          <p><a href="<?php bloginfo('url'); ?>/privacy/">プライバシーポリシー</a></p>
        </li>
        <?php endif; ?>
        <?php if(is_mobile()):?>
        <li><a href="<?php bloginfo('url'); ?>/service/" >サービス</a></li>
        <li><a href="<?php bloginfo('url'); ?>/price/">料金</a></li>
        <li><a href="<?php bloginfo('url'); ?>/service_course/">クリーニングコース</a></li>
        <li><a href="<?php bloginfo('url'); ?>/delivery/">集配サービス</a></li>
        <li><a href="<?php bloginfo('url'); ?>/shop/" >店舗情報</a></li>
        <li><a href="<?php bloginfo('url'); ?>/company/" >会社案内</a></li>
        <li><a href="<?php bloginfo('url'); ?>/newgraduate_list/" >新卒採用情報</a></li>
        <li><a href="<?php bloginfo('url'); ?>/recruit_list/" >中途採用情報</a></li>
        <li><a href="<?php bloginfo('url'); ?>/inquiry/" >お問い合わせ</a></li>
        <li><a href="<?php bloginfo('url'); ?>/privacy/">プライバシーポリシー</a></li>
        <?php endif; ?>
      </ul>
    </nav>
  </div>
  <!-- wrapper -->
  <div class="copy">
    <p>Copyright &copy; Sapporo Hakuyosha. All rights reserved. </p>
  </div>
  <!-- copy --> 
  
</footer>
</div>
<!-- outer -->

<?php if(is_mobile()): ?>
<script>

$(function(){

$("#openMenu").click(function(){
  $("#openMenu").hide();
  $("#closeMenu").show();
  $("#layerMenu").show();
  $("#overray").show();
});
$("#closeMenu").click(function(){
  $("#closeMenu").hide();
  $("#openMenu").show();
  $("#layerMenu").hide();
  $("#overray").hide();
});
$("#layerMenu a").click(function(){
  $("#closeMenu").hide();
  $("#openMenu").show();
  $("#layerMenu").hide();
  $("#overray").hide();
});
$("#overray").click(function(){
  $("#closeMenu").hide();
  $("#openMenu").show();
  $("#layerMenu").hide();
  $("#overray").hide();
});
});
</script>
<?php endif; ?>
<?php wp_footer(); ?>
</div>
<!-- outer -->

</body></html>