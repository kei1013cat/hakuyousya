<section id="page_recruit">
    <section class="recruit_list">
      <div class="wrapper">

      <?php
      $wp_query = new WP_Query();
      $param = array(
        'posts_per_page' => '-1', //表示件数。-1なら全件表示
        'post_status' => 'publish',
        'order' => 'ASC',
        'post_type' => 'recruit'
      );

      $wp_query->query($param);?>
      <?php if($wp_query->have_posts()):?>

        <ul>
          <?php while($wp_query->have_posts()) :?>
          <?php $wp_query->the_post(); ?>

          <li class="cf">
            <div class="title cf">
              <h3><a href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a>
                <span><?php echo get_field('従業員種別'); ?></span>
              </h3>
              <div class="more pc"> <a href="<?php the_permalink(); ?>"><img src="<?php bloginfo('template_url'); ?>/images/recruit_more.svg" width="120" height="29"></a></div>
              <!-- more --></div>
              <!--title -->
              <div class="cont">
                <dl class="cf">
                  <dt>職種</dt>
                  <dd><?php echo get_field('職種'); ?></dd>
                </dl>
                <dl class="cf">
                  <dt>勤務地</dt>
                  <dd><?php echo get_field('勤務地'); ?></dd>
                </dl>
                <dl class="cf">
                  <dt>給与</dt>
                  <dd><?php echo get_field('給与'); ?></dd>
                </dl>
                <dl class="cf">
                  <dt>勤務時間</dt>
                  <dd><?php echo get_field('勤務曜日・時間'); ?></dd>
                </dl>
                <div class="more sp"> <a href="<?php the_permalink(); ?>"><img src="<?php bloginfo('template_url'); ?>/images/recruit_more.svg" width="120" height="29"></a></div>
              </div>
              <!-- cont -->
          </li>

          <?php endwhile; ?>

      </ul>
      </div>
      <!-- wrapper --> 
    </section>
    <?php endif; ?>
    <?php wp_reset_query(); ?>



</section>
