<?php
//-------------------------------------------
// エディタをビジュアルにする
//-------------------------------------------
add_filter('wp_default_editor', create_function('', 'return "tinymce";'));
//-------------------------------------------
// 使用しないメニューを非表示にする
//-------------------------------------------
function remove_admin_menus() {
 
    // level10以外のユーザーの場合
//    if (!current_user_can('level_10')) {
        global $menu;

        // unsetで非表示にするメニューを指定
//        unset($menu[2]);        // ダッシュボード
//      unset($menu[5]);        // 投稿
//        unset($menu[10]);       // メディア
//        unset($menu[20]);       // 固定ページ
        unset($menu[25]);       // コメント
//        unset($menu[60]);       // 外観
//        unset($menu[65]);       // プラグイン
//        unset($menu[70]);       // ユーザー
//        unset($menu[75]);       // ツール
//        unset($menu[80]);       // 設定
//    }
}
add_action('admin_menu', 'remove_admin_menus');

//-------------------------------------------
//管理画面の「見出し１」等を削除する
//-------------------------------------------

function custom_editor_settings( $initArray ){
$initArray['block_formats'] = "段落=p; 見出し1=h3; 見出し2=h4;";
return $initArray;
}
add_filter( 'tiny_mce_before_init', 'custom_editor_settings' );



//-------------------------------------------
//管理画面の「見出し１」等を削除する
//-------------------------------------------
// function TinyMceInitOptions4BrOnly( $initArray )
// {
// 	$initArray = array_merge( $initArray,  array('force_br_newlines' =>  true) );
// 	$initArray = array_merge( $initArray,  array('forced_root_block' => '') );
// 	$initArray = array_merge( $initArray,  array('force_p_newlines' => false) );
// 	return $initArray;
// }

// add_filter('teeny_mce_before_init', 'TinyMceInitOptions4BrOnly');
// add_filter('tiny_mce_before_init', 'TinyMceInitOptions4BrOnly');

//-------------------------------------------
//投稿部分表示カスタマイズ
//-------------------------------------------
function change_post_menu_label() {
	global $menu;
	global $submenu;
	$menu[5][0] = 'お知らせ';
	$submenu['edit.php'][5][0] = 'お知らせ一覧';
	$submenu['edit.php'][10][0] = '新規作成';
	//$submenu['edit.php'][16][0] = 'タグ';
	//echo ”;
}
function change_post_object_label() {
	global $wp_post_types;
	$labels = &$wp_post_types['post']->labels;
	$labels->name = 'お知らせ';
	$labels->singular_name = 'お知らせ';
	$labels->add_new = _x('新規作成', '新着情報');
	$labels->add_new_item = '新しい情報';
	$labels->edit_item = '情報の編集';
	$labels->new_item = '新しい情報';
	$labels->view_item = '新着情報を表示';
	$labels->search_items = '新着情報検索';
	$labels->not_found = '新着情報が見つかりませんでした';
	$labels->not_found_in_trash = 'ゴミ箱の新着情報にも見つかりませんでした';
}
add_action( 'init', 'change_post_object_label' );
add_action( 'admin_menu', 'change_post_menu_label' );

//-------------------------------------------
//カスタム投稿タイプ(キャンペーン情報)
//-------------------------------------------
add_action('init', 'my_custom_init1');
function my_custom_init1()
{
  $labels = array(
    
    'name' => _x('キャンペーン情報', 'post type general name'),
    'singular_name' => _x('キャンペーン情報一覧', 'post type singular name'),
    'add_new' => _x('新規作成', 'camp'),
    'add_new_item' => __('新規作成'),
    'edit_item' => __('キャンペーン情報を編集'),
    'new_item' => __('キャンペーン情報'),
    'view_item' => __('キャンペーン情報一覧を見る'),
    'search_items' => __('キャンペーン情報を探す'),
    'not_found' =>  __('記事はありません'),
    'not_found_in_trash' => __('ゴミ箱に記事はありません'),
    'parent_item_colon' => '',
  'enter_title_here' => _x( 'タイトルを入力', 'camp' ),
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,

    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 6,
    //'supports' => array('title','editor','thumbnail','custom-fields','excerpt','author','trackbacks','comments','revisions','page-attributes'),
  'supports' => array('title','editor','thumbnail'),
    'has_archive' => true
  );
  register_post_type('camp',$args);
}


//-------------------------------------------
//カスタム投稿タイプ(中途採用情報)
//-------------------------------------------
add_action('init', 'my_custom_init2');
function my_custom_init2()
{
  $labels = array(
    'name' => _x('中途採用情報', 'post type general name'),
    'singular_name' => _x('中途採用情報', 'post type singular name'),
    'add_new' => _x('新規作成', 'recruit'),
    'add_new_item' => __('新規作成'),
    'edit_item' => __('記事を編集'),
    'new_item' => __('新しい記事'),
    'view_item' => __('記事を見る'),
    'search_items' => __('記事を探す'),
    'not_found' =>  __('記事はありません'),
    'not_found_in_trash' => __('ゴミ箱に記事はありません'),
    'parent_item_colon' => ''
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,

    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 5,
    'supports' => array('title','editor','thumbnail','custom-fields','excerpt','author','trackbacks','comments','revisions','page-attributes'),
    'has_archive' => true
  );
  register_post_type('recruit',$args);
}


//-------------------------------------------
//カスタム投稿タイプ(新卒採用情報)
//-------------------------------------------
add_action('init', 'my_custom_init5');
function my_custom_init5()
{
  $labels = array(
    'name' => _x('新卒採用情報', 'post type general name'),
    'singular_name' => _x('新卒採用情報', 'post type singular name'),
    'add_new' => _x('新規作成', 'newgraduate'),
    'add_new_item' => __('新規作成'),
    'edit_item' => __('記事を編集'),
    'new_item' => __('新しい記事'),
    'view_item' => __('記事を見る'),
    'search_items' => __('記事を探す'),
    'not_found' =>  __('記事はありません'),
    'not_found_in_trash' => __('ゴミ箱に記事はありません'),
    'parent_item_colon' => ''
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,

    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 5,
    'supports' => array('title','editor','thumbnail','custom-fields','excerpt','author','trackbacks','comments','revisions','page-attributes'),
    'has_archive' => true
  );

  register_post_type('newgraduate',$args);
}


//-------------------------------------------
//カスタム投稿タイプ(トップスライド)
//-------------------------------------------
add_action('init', 'my_custom_init3');
function my_custom_init3()
{
  $labels = array(
    'name' => _x('PC版トップスライド', 'post type general name'),
    'singular_name' => _x('PC版トップスライド', 'post type singular name'),
    'add_new' => _x('新規作成', 'pctopslide'),
    'add_new_item' => __('新規作成'),
    'edit_item' => __('記事を編集'),
    'new_item' => __('新しい記事'),
    'view_item' => __('記事を見る'),
    'search_items' => __('記事を探す'),
    'not_found' =>  __('記事はありません'),
    'not_found_in_trash' => __('ゴミ箱に記事はありません'),
    'parent_item_colon' => ''
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,

    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 5,
    'supports' => array('title','editor','thumbnail','custom-fields','excerpt','author','trackbacks','comments','revisions','page-attributes'),
    'has_archive' => false
  );

  register_post_type('pctopslide',$args);
}

//-------------------------------------------
//カスタム投稿タイプ(トップスライド)
//-------------------------------------------
add_action('init', 'my_custom_init4');
function my_custom_init4()
{
  $labels = array(
    'name' => _x('SP版トップスライド', 'post type general name'),
    'singular_name' => _x('SP版トップスライド', 'post type singular name'),
    'add_new' => _x('新規作成', 'sptopslide'),
    'add_new_item' => __('新規作成'),
    'edit_item' => __('記事を編集'),
    'new_item' => __('新しい記事'),
    'view_item' => __('記事を見る'),
    'search_items' => __('記事を探す'),
    'not_found' =>  __('記事はありません'),
    'not_found_in_trash' => __('ゴミ箱に記事はありません'),
    'parent_item_colon' => ''
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,

    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 5,
    'supports' => array('title','editor','thumbnail','custom-fields','excerpt','author','trackbacks','comments','revisions','page-attributes'),
    'has_archive' => false
  );

  register_post_type('sptopslide',$args);
}

//-------------------------------------------
// アイチャッチ有効化
//-------------------------------------------
//add_theme_support('post-thumbnails');
//-------------------------------------------
// スマホならtrue, タブレット・PCならfalseを返す
//-------------------------------------------

global $is_mobile;
$is_mobile = false;

$useragents = array(
 'iPhone',          // iPhone
 'iPod',            // iPod touch
 'Android',         // 1.5+ Android
 'dream',           // Pre 1.5 Android
 'CUPCAKE',         // 1.5+ Android
 'blackberry9500',  // Storm
 'blackberry9530',  // Storm
 'blackberry9520',  // Storm v2
 'blackberry9550',  // Storm v2
 'blackberry9800',  // Torch
 'webOS',           // Palm Pre Experimental
 'incognito',       // Other iPhone browser
 'webmate'          // Other iPhone browser
 );
 $pattern = '/'.implode('|', $useragents).'/i';
 if( preg_match($pattern, $_SERVER['HTTP_USER_AGENT'])){
	 $is_mobile = preg_match($pattern, $_SERVER['HTTP_USER_AGENT']);
 }

function is_mobile(){
	global $is_mobile;
	return $is_mobile;
	//return false;
}
function is_pc(){
	if(is_mobile()){
		return false;
	}else{
		return true;
	}
}
function mobile_img(){
	if (is_mobile()) {
		echo "_sp";
	}
}
//-------------------------------------------
//カスタム投稿のタイトルを変更
//-------------------------------------------
add_filter( 'enter_title_here', 'custom_enter_title_here', 10, 2 );
function custom_enter_title_here( $enter_title_here, $post ) {
    $post_type = get_post_type_object( $post->post_type );
    if ( isset( $post_type->labels->enter_title_here ) && $post_type->labels->enter_title_here && is_string( $post_type->labels->enter_title_here ) ) {
        $enter_title_here = esc_html( $post_type->labels->enter_title_here );
    }
    return $enter_title_here;
}
//-------------------------------------------
//プレビューボタン非表示
//-------------------------------------------
add_action('admin_print_styles', 'admin_preview_css_custom');
function admin_preview_css_custom() {
   echo '<style>#preview-action {display: none;}</style>';
   echo '<style>#message a {display: none;}</style>';
   echo '<style>#message a {display: none;}</style>';
}

//-------------------------------------------
//記事画像パスを相対パスで利用する
//-------------------------------------------

function delete_host_from_attachment_url( $url ) {
    $regex = '/^http(s)?:\/\/[^\/\s]+(.*)$/';
    if ( preg_match( $regex, $url, $m ) ) {
        $url = $m[2];
    }
    return $url;
}
add_filter( 'wp_get_attachment_url', 'delete_host_from_attachment_url' );
add_filter( 'attachment_link', 'delete_host_from_attachment_url' );

function replaceImagePath($arg) {
	$content = str_replace('"images/', '"' . get_bloginfo('template_directory') . '/images/', $arg);
	return $content;
}  
add_action('the_content', 'replaceImagePath');
register_sidebar();


//-------------------------------------------
//サムネイルサイズを追加
//-------------------------------------------

function thumbAdd() {
add_theme_support( 'post-thumbnails' ); //テーマをサムネイル表示に対応させる
set_post_thumbnail_size(100, 100, true);//サムネイルの画像サイズを設定
add_image_size( 'works_list_size', 455,310, true ); //事例の画像サイズ
add_image_size( 'works_single_size', 800,540, true ); //事例の画像サイズ
add_image_size( 'works_singlelist_size', 390,266, true ); //事例の画像サイズ
add_image_size( 'topslide_pc', 1000,380); //PC版トップスライド用の画像サイズ
add_image_size( 'topslide_sp', 600,700); //SP版トップスライド用の画像サイズ

}
add_action( 'after_setup_theme', 'thumbAdd' );


//本文抜粋を取得する関数
function get_the_custom_excerpt($content, $length) {
  $length = ($length ? $length : 70);//デフォルトの長さを指定する
  $content =  preg_replace('/<!--more-->.+/is',"",$content); //moreタグ以降削除
  $content =  strip_shortcodes($content);//ショートコード削除
  $content =  strip_tags($content);//タグの除去
  $content =  str_replace("&nbsp;","",$content);//特殊文字の削除（今回はスペースのみ）
  $content =  mb_substr($content,0,$length);//文字列を指定した長さで切り取る
  return $content;
}



//-------------------------------------------
//ページャー
//-------------------------------------------
function bmPageNaviGallery() {
  global $wp_rewrite;
  global $wp_query;
  global $paged;
 
  $paginate_base = get_pagenum_link(1);
  if (strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()) {
    $paginate_format = '';
    $paginate_base = add_query_arg('page', '%#%');
  } else {
    $paginate_format = (substr($paginate_base, -1 ,1) == '/' ? '' : '') .
    untrailingslashit('page/%#%', 'paged');
    $paginate_base .= '%_%';  
  }
  $cat = "";
  if(!empty($_GET['cat'])){
	$cat = "&cat=".htmlspecialchars($_GET['cat'], ENT_QUOTES);
  }
  //$paginate_base = "./page/%#%".$cat;
  $result = paginate_links( array(
   // 'base' => $paginate_base,
    'format' => $paginate_format,
    'total' => $wp_query->max_num_pages,
    'mid_size' => 4,
    'prev_text' => '&lt;&lt;',
    'next_text' => '&gt;&gt;',
    'current' => ($paged ? $paged : 1),
  ));
 
  return $result;
}

//-------------------------------------------
// 郵便番号自動
//-------------------------------------------
function ajaxzip3_scripts() {
    wp_enqueue_script( 'ajaxzip3-script', get_bloginfo('template_directory') . '/js/ajaxzip3.js', array( 'jquery' ), '20140807', true );
}
add_action( 'wp_enqueue_scripts', 'ajaxzip3_scripts' );


?>