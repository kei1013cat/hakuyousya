<div id="sidebar">

<?php if($post->post_type =="blog"):?>

  <h3>最新の記事</h3>
  <ul class="cf">
    <?php
		$wp_query = new WP_Query();
		$param = array(
			'posts_per_page' => '10', //表示件数。-1なら全件表示
			'post_status' => 'publish',
			'orderby' => 'date', //ID順に並び替え
			'post_type' => 'blog',		
			'order' => 'DESC'
		);
		$wp_query->query($param);?>
    <?php if($wp_query->have_posts()): while($wp_query->have_posts()) : $wp_query->the_post(); ?>
    <li> <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>">
      <?php $title= mb_substr($post->post_title,0,30); echo $title;?>
      </a></li>
    <?php endwhile; ?>
    <?php endif; ?>
  </ul>
  <?php wp_reset_query(); ?>

<?php else: ?>
  <h3>最新の記事</h3>
  <ul class="cf">
    <?php
		$wp_query = new WP_Query();
		$param = array(
			'posts_per_page' => '10', //表示件数。-1なら全件表示
			'post_status' => 'publish',
			'orderby' => 'date', //ID順に並び替え
			'order' => 'DESC'
		);
		$wp_query->query($param);?>
    <?php if($wp_query->have_posts()): while($wp_query->have_posts()) : $wp_query->the_post(); ?>
    <li> <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>">
      <?php $title= mb_substr($post->post_title,0,30); echo $title;?>
      </a></li>
    <?php endwhile; ?>
    <?php endif; ?>
  </ul>
  <?php wp_reset_query(); ?>
<?php endif; ?>  
  
  
  
</div>
<!-- sidebar -->