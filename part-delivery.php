<section class="delivery_info">
  <div class="wrapper">
  <p>衣類のケアに関する知識を持つ白洋舍の集配担当者がお客さまのお宅を訪問し、クリーニング品のお預かり、お届けを行うサービスです。定期的な訪問サービスのほか、必要に応じて電話やネットからお申込みいただくこともできます。</p>
    </div>
  </section>
  <section class="point">
  <div class="wrapper">
  <img src="<?php bloginfo('template_url'); ?>/images/delivery_point<?php mobile_img();?>.jpg"></div>
  </section>

  <section class="convenient">
  <div class="wrapper">
    <h3>こんな場合に便利です！</h3>
    <ul>
      <li>ワイシャツなどを定期的にクリーニングに出したいがお店に行くのが面倒</li>
      <li>クリーニング品の量が多い場合や、じゅうたん、布団、カーテンなどお店に持っていくのが大変なものの場合<span class="kome">※クリーニング品1点でも集配に伺います！</span></li>
      <li>最適なクリーニング方法について相談したい。しみ抜きやオプション加工などについても相談したい</li>
    </ul>
  </div>
  <!-- wrapper -->
  </section>

  <section class="step">
  <h3><span class="line"><span class="top">か</span><span class="bottom">ん</span><span class="top">た</span><span class="bottom">ん</span>４ステップ</span></h3>
  <div class="wrapper">
    <ul class="cf">
    <li>
    	<h4>STEP</h4>
      <dl class="cf">
        <dt>
          <img src="<?php bloginfo('template_url'); ?>/images/delivery_step1.jpg" alt="step1 | 札幌白洋舍">
        </dt>
        <dd>最寄りの支店までお電話またはインターネットにてお申し込みください。</dd>
      </dl>
      </li>
      <li>
      <h4>STEP</h4>
      <dl class="cf">
        <dt>
          <img src="<?php bloginfo('template_url'); ?>/images/delivery_step2.jpg" alt="step2 | 札幌白洋舍">
        </dt>
        <dd>集配スタッフがご自宅までお伺いいたします。お品物をお渡しください。</dd>
      </dl>
      </li>
      <li>
      <h4>STEP</h4>
      <dl class="cf">
        <dt>
          <img src="<?php bloginfo('template_url'); ?>/images/delivery_step3.jpg" alt="step3 | 札幌白洋舍">
        </dt>
        <dd>大切な一着を上質クリーニングでケアいたします。</dd>
      </dl>
      </li>
      <li>
      <h4>STEP</h4>
      <dl class="cf">
        <dt>
          <img src="<?php bloginfo('template_url'); ?>/images/delivery_step4.jpg" alt="step4 | 札幌白洋舍">
        </dt>
        <dd>ご都合のよい日時を打ち合わせの上、ご自宅までお届けにあがります。</dd>
      </dl>
      </li>
      </ul>
  </section>

  <p class="linkbtn2"><a href="<?php bloginfo('url'); ?>/delivery_form/">集配を依頼する</a></p>
  <section class="shop">
   <div class="wrapper">
   <h3 class="headline02">営業所一覧</h3>
    <ul>
      <li class="cf">
        <h3>琴似営業所</h3>
       <div class="cont">
          <dl class="cf">
            <dt>住所</dt>
            <dd>札幌市西区山の手6条1丁目3-34 </dd>
          </dl>
          <dl class="cf">
            <dt>電話番号</dt>
            <dd>011-611-4369</dd>
          </dl>
          <dl class="cf">
            <dt>営業時間</dt>
            <dd>8：00～17：00&nbsp;(年末・年始等営業時間変更有り)</dd>
          </dl>
          <dl class="cf">
            <dt>休日</dt>
            <dd>土・日</dd>
          </dl>
<dl class="cf">
            <dt>その他</dt>
            <dd>年末・年始等営業時間変更有り</dd>
          </dl>
        </div>
        <div class="area">
        <h4>対応可能エリア</h4>
        <p>(札幌市) 中央区、西区、豊平区、手稲区、小樽市、岩見沢市</p>
        </div><!-- area -->
      </li>
      
      
      
      
      <li class="cf">
        <h3>北営業所</h3>
       <div class="cont">
          <dl class="cf">
            <dt>住所</dt>
            <dd>札幌市東区本町2条5丁目4-10</dd>
          </dl>
          <dl class="cf">
            <dt>電話番号</dt>
            <dd>011-784-0339</dd>
          </dl>
          <dl class="cf">
            <dt>営業時間</dt>
            <dd>8：00～17：00&nbsp;(年末・年始等営業時間変更有り)</dd>
          </dl>
          <dl class="cf">
            <dt>休日</dt>
            <dd>土・日</dd>
          </dl>
          <dl class="cf">
            <dt>その他</dt>
            <dd>年末・年始・夏期休業有り</dd>
          </dl>
        </div>
        <div class="area">
        <h4>対応可能エリア</h4>
        <p>(札幌市) 東区、北区、清田区、厚別区、白石区、石狩市、北広島市、江別市</p>
        </div><!-- area -->
      </li>
      
      
      
      <li class="cf">
        <h3>真駒内営業所</h3>
       <div class="cont">
          <dl class="cf">
            <dt>住所</dt>
            <dd>札幌市南区真駒内泉町2丁目2-7 </dd>
          </dl>
          <dl class="cf">
            <dt>電話番号</dt>
            <dd>011-581-3003</dd>
          </dl>
          <dl class="cf">
            <dt>営業時間</dt>
            <dd>8：00～17：00&nbsp;(年末・年始等営業時間変更有り)</dd>
          </dl>
          <dl class="cf">
            <dt>休日</dt>
            <dd>土･日</dd>
          </dl>
<dl class="cf">
            <dt>その他</dt>
            <dd>年末・年始・夏期休業有り</dd>
          </dl>
        </div>
        <div class="area">
        <h4>対応可能エリア</h4>
        <p>(札幌市) 豊平区、南区</p>
        </div><!-- area -->
      </li>
      
      
    </ul>
    <p class="linkbtn2"><a href="<?php bloginfo('url'); ?>/delivery_form/">集配を依頼する</a></p>
  </div>
  <!-- wrapper --> 
  </section>
  </div>
  <!-- wrapper --> 
</section>
