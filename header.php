<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta name="google-site-verification" content="kiimkDsD27u4V-dM1a_L7E5U5Q2iArVHgGkwQIplxy4" />
<?php
  $url = $_SERVER['REQUEST_URI'];
?>
<meta name="format-detection" content="telephone=no">
<?php if(is_pc()): ?>
<meta content="width=1000" name="viewport">
<?php else: ?>
<meta name="viewport" content="width=device-width">
<?php endif; ?>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title>
<?php if(is_page() && $post->post_name != 'home'){ wp_title('');echo ' | '; } ?>
<?php bloginfo('name'); ?>
</title>
<meta name="keywords" content="白洋舍,白洋舎,札幌白洋舎,札幌白洋舍株式会社,クリーニング,ハウスクリーニング,靴,バッグ,北海道" />

<?php wp_head(); ?>
<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<?php if(is_mobile()){ ?>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/sp.css?v=20190401" type="text/css">
<?php }else{ ?>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/pc.css?v=20190401" type="text/css">
<?php } ?>
<script src="<?php bloginfo('template_url'); ?>/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/smoothScroll.js"></script>

<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico" >

<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/pulldown.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/rollover.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/sp_switch.js"></script>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/izimodal/css/iziModal.min.css" type="text/css">
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/izimodal/js/iziModal.min.js"></script>

<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/smoothScroll.js"></script>





<?php if(is_pc()):?>
<!--[if lt IE 9]>
<script src="<?php bloginfo('template_url'); ?>/js/html5.js"></script>
<![endif]-->
<?php endif; ?>

<script>
    $(function(){
        $(".acMenu dt").on("click", function() {
            $(this).next().slideToggle('fast');
        });
    });
</script>

<script type="text/javascript">
jQuery(function( $ ) {
    jQuery( 'input[name="zip2"]' ).keyup( function( e ) {
        AjaxZip3.zip2addr('zip1','zip2','address1','address2');
    } )
} );
</script>

<script>
  jQuery(function($) {
    <?php if ($_GET["str"] != NULL): ?>
        $('#entryseat').prepend('<span><?php echo $_GET['str']; ?></span>');
        $('#str').val('<?php echo $_GET['str']; ?>');
        $('#strnm').hide();

        $( '#mw_wp_form_mw-wp-form-111 .year' ).val( '1970' );
        $( '#mw_wp_form_mw-wp-form-111 .months' ).val( '01' );
        $( '#mw_wp_form_mw-wp-form-111 .day' ).val( '01' );

    <?php endif ?>



  });
</script>

<script>
$(function() {

$(document).on('click', '.open-options1', function(event) {
  event.preventDefault();
  $('.modal-options1').iziModal('open');
});
$('.modal-options1').iziModal({
  headerColor: '#5796f2', //ヘッダー部分の色
  width: 600, //横幅
  overlayColor: 'rgba(0, 0, 0, 0.5)', //モーダルの背景色
  fullscreen: true, //全画面表示
  transitionIn: 'fadeInUp', //表示される時のアニメーション
  transitionOut: 'fadeOutDown' //非表示になる時のアニメーション
});


$(document).on('click', '.open-options2', function(event) {
  event.preventDefault();
  $('.modal-options2').iziModal('open');
});
$('.modal-options2').iziModal({
  headerColor: '#5796f2', //ヘッダー部分の色
  width: 600, //横幅
  overlayColor: 'rgba(0, 0, 0, 0.5)', //モーダルの背景色
  fullscreen: true, //全画面表示
  transitionIn: 'fadeInUp', //表示される時のアニメーション
  transitionOut: 'fadeOutDown' //非表示になる時のアニメーション
});


$(document).on('click', '.open-options3', function(event) {
  event.preventDefault();
  $('.modal-options3').iziModal('open');
});
$('.modal-options3').iziModal({
  headerColor: '#5796f2', //ヘッダー部分の色
  width: 600, //横幅
  overlayColor: 'rgba(0, 0, 0, 0.5)', //モーダルの背景色
  fullscreen: true, //全画面表示
  transitionIn: 'fadeInUp', //表示される時のアニメーション
  transitionOut: 'fadeOutDown' //非表示になる時のアニメーション
});

});

</script>

<script type="text/javascript">
$(function(){
    var $setElm = $('.wideslider'),
	<?php if(is_pc()): ?>
    baseWidth = 1000,
    baseHeight = 380,
	<?php else: ?>
    baseWidth = 600,
    baseHeight = 700,
	<?php endif; ?>
    slideSpeed = 500,
    delayTime = 5000,
    easing = 'linear',
 
    autoPlay = '1', // notAutoPlay = '0'
 
    btnOpacity = 0.5,
    pnOpacity = 0.5;
 
    $setElm.each(function(){
        var targetObj = $(this);
        var widesliderWidth = baseWidth;
        var widesliderHeight = baseHeight;
        var wsSetTimer;
 
        targetObj.children('ul').wrapAll('<div class="wideslider_base"><div class="wideslider_wrap"></div><div class="slider_prev"></div><div class="slider_next"></div></div>');
 
        var findBase = targetObj.find('.wideslider_base');
        var findWrap = targetObj.find('.wideslider_wrap');
        var findPrev = targetObj.find('.slider_prev');
        var findNext = targetObj.find('.slider_next');
 
        var baseListWidth = baseWidth;
        var baseListCount = findWrap.children('ul').children('li').length;
 
        var baseWrapWidth = (baseListWidth)*(baseListCount);
 
        var pagination = $('<div class="pagination"></div>');
        targetObj.append(pagination);
        var baseList = findWrap.children('ul').children('li');
        baseList.each(function(i){
            $(this).css({width:(baseWidth),height:(baseHeight)});
            pagination.append('<a href="javascript:void(0);" class="pn'+(i+1)+'"></a>');
        });
 
        var pnPoint = pagination.children('a');
        var pnFirst = pagination.children('a:first');
        var pnLast = pagination.children('a:last');
        var pnCount = pagination.children('a').length;
        pnPoint.css({opacity:(pnOpacity)}).hover(function(){
            $(this).stop().animate({opacity:'1'},300);
        }, function(){
            $(this).stop().animate({opacity:(pnOpacity)},300);
        });
        pnFirst.addClass('active');
        pnPoint.click(function(){
            if(autoPlay == '1'){clearInterval(wsSetTimer);}
            var setNum = pnPoint.index(this);
            var moveLeft = ((baseListWidth)*(setNum))+baseWrapWidth;
            findWrap.stop().animate({left: -(moveLeft)},slideSpeed,easing);
            pnPoint.removeClass('active');
            $(this).addClass('active');
            if(autoPlay == '1'){wsTimer();}
        });
 
        var makeClone = findWrap.children('ul');
        makeClone.clone().prependTo(findWrap);
        makeClone.clone().appendTo(findWrap);
 
        var allListWidth = findWrap.children('ul').children('li').width();
        var allListCount = findWrap.children('ul').children('li').length;
 
        var allLWrapWidth = (allListWidth)*(allListCount);
        var windowWidth = $(window).width();
        var posAdjust = ((windowWidth)-(baseWidth))/2;
 
        findBase.css({left:(posAdjust),width:(baseWidth),height:(baseHeight)});
        findPrev.css({left:-(baseWrapWidth),width:(baseWrapWidth),height:(baseHeight),opacity:(btnOpacity)});
        findNext.css({right:-(baseWrapWidth),width:(baseWrapWidth),height:(baseHeight),opacity:(btnOpacity)});
        $(window).bind('resize',function(){
            var windowWidth = $(window).width();
            var posAdjust = ((windowWidth)-(baseWidth))/2;
            findBase.css({left:(posAdjust)});
            findPrev.css({left:-(posAdjust),width:(posAdjust)});
            findNext.css({right:-(posAdjust),width:(posAdjust)});
        }).resize();
 
        findWrap.css({left:-(baseWrapWidth),width:(allLWrapWidth),height:(baseHeight)});
        findWrap.children('ul').css({width:(baseWrapWidth),height:(baseHeight)});
 
        var posResetNext = -(baseWrapWidth)*2;
        var posResetPrev = -(baseWrapWidth)+(baseWidth);
 
        if(autoPlay == '1'){wsTimer();}
 
        function wsTimer(){
            wsSetTimer = setInterval(function(){
                findNext.click();
            },delayTime);
        }
        findNext.click(function(){
            findWrap.not(':animated').each(function(){
                if(autoPlay == '1'){clearInterval(wsSetTimer);}
                var posLeft = parseInt($(findWrap).css('left'));
                var moveLeft = ((posLeft)-(baseWidth));
                findWrap.stop().animate({left:(moveLeft)},slideSpeed,easing,function(){
                    var adjustLeft = parseInt($(findWrap).css('left'));
                    if(adjustLeft == posResetNext){
                        findWrap.css({left: -(baseWrapWidth)});
                    }
                });
                var pnPointActive = pagination.children('a.active');
                pnPointActive.each(function(){
                    var pnIndex = pnPoint.index(this);
                    var listCount = pnIndex+1;
                    if(pnCount == listCount){
                        pnPointActive.removeClass('active');
                        pnFirst.addClass('active');
                    } else {
                        pnPointActive.removeClass('active').next().addClass('active');
                    }
                });
                if(autoPlay == '1'){wsTimer();}
            });
        }).hover(function(){
            $(this).stop().animate({opacity:((btnOpacity)+0.1)},100);
        }, function(){
            $(this).stop().animate({opacity:(btnOpacity)},100);
        });
 
        findPrev.click(function(){
            findWrap.not(':animated').each(function(){
                if(autoPlay == '1'){clearInterval(wsSetTimer);}
                var posLeft = parseInt($(findWrap).css('left'));
                var moveLeft = ((posLeft)+(baseWidth));
                findWrap.stop().animate({left:(moveLeft)},slideSpeed,easing,function(){
                    var adjustLeft = parseInt($(findWrap).css('left'));
                    var adjustLeftPrev = (posResetNext)+(baseWidth);
                    if(adjustLeft == posResetPrev){
                        findWrap.css({left: (adjustLeftPrev)});
                    }
                });
                var pnPointActive = pagination.children('a.active');
                pnPointActive.each(function(){
                    var pnIndex = pnPoint.index(this);
                    var listCount = pnIndex+1;
                    if(1 == listCount){
                        pnPointActive.removeClass('active');
                        pnLast.addClass('active');
                    } else {
                        pnPointActive.removeClass('active').prev().addClass('active');
                    }
                });
                if(autoPlay == '1'){wsTimer();}
            });
        }).hover(function(){
            $(this).stop().animate({opacity:((btnOpacity)+0.1)},100);
        }, function(){
            $(this).stop().animate({opacity:(btnOpacity)},100);
        });
    });
});
</script>

<?php if(is_mobile()): ?>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/slider-pro/dist/css/slider-pro.css">
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/slider-pro/dist/js/jquery.sliderPro.js"></script>
<script>
  $( document ).ready(function( $ ) {
  $( '#my-slider' ).sliderPro({
    width: "100%",
    height: 800,
    aspectRatio: 2,
    arrows: false,
    buttons: false,
    autoplay: true,
    autoHeight: true,
    loop: true,
    visibleSize: '100%',
    forceSize: 'fullWidth',
    responsive:true
  });
});
</script>
<?php endif; ?>

<?php if(is_pc()): ?>
<script type="text/javascript">
$(function() {
  $('.tab1 li').click(function() {
    var index = $('.tab1 li').index(this);
    $('.content1 li').css('display','none');
    $('.content1 li').eq(index).css('display','block');
    $('.tab1 li').removeClass('select');
    $(this).addClass('select')
  });
  $('.tab2 li').click(function() {
    var index = $('.tab2 li').index(this);
    $('.content2 li').css('display','none');
    $('.content2 li').eq(index).css('display','block');
    $('.tab2 li').removeClass('select');
    $(this).addClass('select')
  });
  $('.tab3 li').click(function() {
    var index = $('.tab3 li').index(this);
    $('.content3 li').css('display','none');
    $('.content3 li').eq(index).css('display','block');
    $('.tab3 li').removeClass('select');
    $(this).addClass('select')
  });
  $('.tab4 li').click(function() {
    var index = $('.tab4 li').index(this);
    $('.content4 li').css('display','none');
    $('.content4 li').eq(index).css('display','block');
    $('.tab4 li').removeClass('select');
    $(this).addClass('select')
  });
  $('.tab5 li').click(function() {
    var index = $('.tab5 li').index(this);
    $('.content5 li').css('display','none');
    $('.content5 li').eq(index).css('display','block');
    $('.tab5 li').removeClass('select');
    $(this).addClass('select')
  });
  $('.tab6 li').click(function() {
    var index = $('.tab6 li').index(this);
    $('.content6 li').css('display','none');
    $('.content6 li').eq(index).css('display','block');
    $('.tab6 li').removeClass('select');
    $(this).addClass('select')
  });
  $('.tab7 li').click(function() {
    var index = $('.tab7 li').index(this);
    $('.content7 li').css('display','none');
    $('.content7 li').eq(index).css('display','block');
    $('.tab7 li').removeClass('select');
    $(this).addClass('select')
  });
  $('.tab8 li').click(function() {
    var index = $('.tab8 li').index(this);
    $('.content8 li').css('display','none');
    $('.content8 li').eq(index).css('display','block');
    $('.tab8 li').removeClass('select');
    $(this).addClass('select')
  });
  $('.tab9 li').click(function() {
    var index = $('.tab9 li').index(this);
    $('.content9 li').css('display','none');
    $('.content9 li').eq(index).css('display','block');
    $('.tab9 li').removeClass('select');
    $(this).addClass('select')
  });
  $('.tab10 li').click(function() {
    var index = $('.tab10 li').index(this);
    $('.content10 li').css('display','none');
    $('.content10 li').eq(index).css('display','block');
    $('.tab10 li').removeClass('select');
    $(this).addClass('select')
  });
  $('.tab11 li').click(function() {
    var index = $('.tab11 li').index(this);
    $('.content11 li').css('display','none');
    $('.content11 li').eq(index).css('display','block');
    $('.tab11 li').removeClass('select');
    $(this).addClass('select')
  });
  $('.tab12 li').click(function() {
    var index = $('.tab12 li').index(this);
    $('.content12 li').css('display','none');
    $('.content12 li').eq(index).css('display','block');
    $('.tab12 li').removeClass('select');
    $(this).addClass('select')
  });
  $('.tab13 li').click(function() {
    var index = $('.tab13 li').index(this);
    $('.content13 li').css('display','none');
    $('.content13 li').eq(index).css('display','block');
    $('.tab13 li').removeClass('select');
    $(this).addClass('select')
  });
  $('.tab14 li').click(function() {
    var index = $('.tab14 li').index(this);
    $('.content14 li').css('display','none');
    $('.content14 li').eq(index).css('display','block');
    $('.tab14 li').removeClass('select');
    $(this).addClass('select')
  });
  $('.tab15 li').click(function() {
    var index = $('.tab15 li').index(this);
    $('.content15 li').css('display','none');
    $('.content15 li').eq(index).css('display','block');
    $('.tab15 li').removeClass('select');
    $(this).addClass('select')
  });
  $('.tab16 li').click(function() {
    var index = $('.tab16 li').index(this);
    $('.content16 li').css('display','none');
    $('.content16 li').eq(index).css('display','block');
    $('.tab16 li').removeClass('select');
    $(this).addClass('select')
  });
});
</script>
<script type="text/javascript">

$(function() {
  
  //ロード or スクロールされると実行
  $(window).on('load scroll', function(){
    
    //ヘッダーの高さ分(80px)スクロールするとfixedクラスを追加
    if ($(window).scrollTop() > 300) {
      $('.price_menu').addClass('fixed')
      .stop().animate({'top' : '0px'}, 1000);
    $('.price').addClass('fixed');
    } else {
      //80px以下だとfixedクラスを削除
      $('.price_menu').removeClass('fixed');
    $('.price').removeClass('fixed');
    }
    
  });
  
});
</script>
<?php else: ?>
<script type="text/javascript">
  $(function(){
  $("#acMenu dt").on("click", function() {
    $(this).next().slideToggle(); 
    // activeが存在する場合
    if ($(this).children(".accordion_icon").hasClass('active')) {     
      // activeを削除
      $(this).children(".accordion_icon").removeClass('active');        
    }
    else {
      // activeを追加
      $(this).children(".accordion_icon").addClass('active');     
    }     
  });
});
</script>
<?php endif; ?>


</head>
<?php
	$body_id = "";
	if ( is_home() ) {
		$body_id = ' id="page_index"';
	}else if ($post->post_type == 'jirei') {
		$body_id = ' id="page_'.$post->post_type.'" class="subpage"';
	}else if ( is_page() ) {
		$body_id = ' id="page_'.get_post($post->post_parent)->post_name.'" class="subpage"';
	}else if (  is_post_type_archive('works') || is_singular('works') ) {
		$body_id = ' id="page_works" class="subpage"';
	}else if ( is_single() ) {
		$body_id = ' id="page_single" class="subpage"';
	}else if ( is_archive() ) {
		$body_id = ' id="page_archive" class="subpage"';
	}
?>

<?php
    $parent_id = $post->post_parent; // 親ページのIDを取得
    $parent_slug = get_post($parent_id)->post_name; // 親ページのスラッグを取得
?>


<body<?php echo $body_id; ?>>

<div id="outer">
<header class="global">
  <div class="wrapper cf">


<?php if(is_pc()):?>

    <h1><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/header_logo.png" alt="札幌白洋舍 | 札幌のクリーニングなら札幌白洋舎にお任せください" /></a></h1>
    <nav>
      <ul class="cf">
        <?php if($post->post_name =='service' || $post->post_name =='service_course'): ?>
       	  <li><a class="hover" href="<?php bloginfo('url'); ?>/service/" >サービス<span>SERVICE</span></a></li>
        <?php else: ?>
          <li><a href="<?php bloginfo('url'); ?>/service/" >サービス<span>SERVICE</span></a></li>
        <?php endif; ?>

        <?php if($post->post_name =='price'): ?>
          <li><a class="hover" href="<?php bloginfo('url'); ?>/price/" >料金<span>PRICE</span></a></li>
        <?php else: ?>
          <li><a href="<?php bloginfo('url'); ?>/price/" >料金<span>PRICE</span></a></li>
        <?php endif; ?>

        <?php if($post->post_name =='shop'): ?>
          <li><a class="hover" href="<?php bloginfo('url'); ?>/shop/" >店舗情報<span>SHOP</span></a></li>
        <?php else: ?>
          <li><a href="<?php bloginfo('url'); ?>/shop/" >店舗情報<span>SHOP</span></a></li>
        <?php endif; ?>

        <?php if($post->post_name =='delivery' || $post->post_name =='delivery_form' || $parent_slug =='delivery_form' ): ?>
          <li><a class="hover" href="<?php bloginfo('url'); ?>/delivery/" >集配サービス<span>DELIVERY</span></a></li>
        <?php else: ?>
          <li><a href="<?php bloginfo('url'); ?>/delivery/" >集配サービス<span>DELIVERY</span></a></li>
        <?php endif; ?>

        <?php if($post->post_name =='company' || $post->post_name =='business'): ?>
          <li><a class="hover" href="<?php bloginfo('url'); ?>/company/" >会社案内<span>COMPANY</span></a></li>
        <?php else: ?>
          <li><a href="<?php bloginfo('url'); ?>/company/" >会社案内<span>COMPANY</span></a></li>
        <?php endif; ?>
        
        <?php if($post->post_name =='newgraduate_list' || $post->post_name =='newgraduate' || (is_single() && ($post->post_type =="newgraduate"))): ?>
          <li><a class="hover" href="<?php bloginfo('url'); ?>/newgraduate_list/" >新卒採用情報<span>RECRUIT</span></a></li>
        <?php else: ?>
          <li><a href="<?php bloginfo('url'); ?>/newgraduate_list/" >新卒採用情報<span>RECRUIT</span></a></li>
        <?php endif; ?>

        <?php if($post->post_name =='recruit_list' || $post->post_name =='recruit' || (is_single() && ($post->post_type =="recruit"))): ?>
          <li><a class="hover" href="<?php bloginfo('url'); ?>/recruit_list/" >中途採用情報<span>RECRUIT</span></a></li>
        <?php else: ?>
          <li><a href="<?php bloginfo('url'); ?>/recruit_list/" >中途採用情報<span>RECRUIT</span></a></li>
        <?php endif; ?>
        
        
      </ul>
      <p class="contact"><a href="<?php bloginfo('url'); ?>/inquiry/" ><img src="<?php bloginfo('template_url'); ?>/images/nav_contact.jpg" alt="札幌白洋舍 | お問い合わせ" /></a></p>
    </nav>
  </div>
  <!-- wrapper -->

  <?php endif; ?>
  <?php if(is_mobile()):?>

  <h1><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/header_logo.png" alt="札幌白洋舍 | Sapporo Hakuyosha" /></a></h1>

  <div class="menu cf">
    <p id="closeMenu"> <a href="#"><img src="<?php bloginfo('template_url'); ?>/images/header_close.png" ></a> </p>
    <p id="openMenu"> <a href="#"> <img src="<?php bloginfo('template_url'); ?>/images/header_menu.png"> </a> </p>
  </div>
  <!-- menu -->
  <div id="layerMenu">
    <nav>
      <ul>
        <li><a href="<?php bloginfo('url'); ?>/" >トップ</a></li>
        <li><a href="<?php bloginfo('url'); ?>/service/" >サービス</a></li>
        <li><a href="<?php bloginfo('url'); ?>/price/" >料金</a></li>
        <li><a href="<?php bloginfo('url'); ?>/service_course/" >クリーニングコース</a></li>
        <li><a href="<?php bloginfo('url'); ?>/delivery/" >集配サービス</a></li>
        <li><a href="<?php bloginfo('url'); ?>/shop/" >店舗情報</a></li>
        <li><a href="<?php bloginfo('url'); ?>/company/" >会社案内</a></li>
        <li><a href="<?php bloginfo('url'); ?>/newgraduate_list/" >新卒採用情報</a></li>
        <li><a href="<?php bloginfo('url'); ?>/recruit_list/" >中途採用情報</a></li>
        <li><a href="<?php bloginfo('url'); ?>/inquiry/" >お問い合わせ</a></li>
        <li><a href="<?php bloginfo('url'); ?>/privacy/" >プライバシーポリシー</a></li>
      </ul>
    </nav>
  </div>
  <!-- layerMenu -->
  <div id="overray"> </div>
  <?php endif;?>
  </header>
<div id="main_contents" class="cf">
