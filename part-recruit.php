<section id="page_recruit">
    <section class="recruit_info">
      <div class="wrapper">
       
        <h3 class="headline01">札幌白洋舍　札幌</h3>
        <table class="style01">
          <tbody>
            <tr>
              <th>勤務地</th>
              <td>北海道札幌市西区山の手6条1丁目3-34<br><a href="https://www.google.co.jp/maps/place/%E3%80%92063-0006+%E5%8C%97%E6%B5%B7%E9%81%93%E6%9C%AD%E5%B9%8C%E5%B8%82%E8%A5%BF%E5%8C%BA%E5%B1%B1%E3%81%AE%E6%89%8B%EF%BC%96%E6%9D%A1%EF%BC%91%E4%B8%81%E7%9B%AE%EF%BC%93%E2%88%92%EF%BC%93%EF%BC%94/@43.0757888,141.29177,17z/data=!3m1!4b1!4m5!3m4!1s0x5f0b2837dce9f18d:0x8dc79bac4ce5e57b!8m2!3d43.0757888!4d141.2939587">札幌白洋舍　札幌の地図</a></td>
            </tr>
            <tr>
              <th>勤務曜日・時間</th>
              <td>月～金　8:00～18:00<br> （時期により変動有り、シフト制）</td>
            </tr>
            <tr>
              <th>資格</th>
              <td> 未経験者大歓迎です！<br>フリーター、学生、主婦・主夫大歓迎！<br>日中の短時間勤務OK!<br><br>
              時間曜日などあなたの希望ご相談ください！<br><br>
              幅広い年齢層のスタッフが活躍中です♪<br>まずはお気軽にご応募ください！</td>
            </tr>
            <tr>
              <th>休日</th>
              <td>土・日・年末年始・夏季</td>
            </tr>
            <tr>
              <th>待遇</th>
              <td>交通費支給(規定あり)<br>昇給あり<br>有給休暇あり<br>社会保険完備<br>従業員割引あり<br>制服貸与<br>社内資格制度あり(資格取得者は時給UP！)<br>社員登用あり<br></td>
            </tr>
            <tr>
              <th>ポイント</th>
              <td>■働きやすいシフト制♪<br>&nbsp;&nbsp;&nbsp;&nbsp;シフトはご相談いただけます。<br>■未経験でも大丈夫！<br>&nbsp;&nbsp;&nbsp;&nbsp;前職経験は問いません。<br>■働きやすい職場です♪ <br>&nbsp;&nbsp;&nbsp;&nbsp;長期でお勤めのスタッフも多数いらっしゃいます。<br><br>まずはお気軽にご応募ください♪</td>
            </tr>
            </tbody>
        </table>
<p class="linkbtn2"><a href="<?php bloginfo('url'); ?>/recruit_form/">この求人に応募する</a></p>
      </div>
      <!-- wrapper --> 
    </section>




</section>