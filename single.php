<?php get_header(); ?>

<div id="contents">
	<?php include (TEMPLATEPATH . '/part-title.php'); ?>
	<section class="news_entry bg_beige">
		<div class="wrapper cf">
			<div class="left_contents">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<article <?php post_class(); ?>>
					<div class="entry-header">
						<h3 class="entry-title">
							<?php the_title(); ?>
						</h3>
						<p>
							<time class="entry-date" datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate="<?php the_time( 'Y-m-d' ); ?>">
								<?php the_time( 'Y.m.d'  ); ?>
							</time>
							<?php
								$category = get_the_category();
								$cat_name = $category[0]->cat_name;
							?>
							<?php if($cat_name!='未分類'):?>
							<span class="cat">
							<?php echo $cat_name; ?>
							</span>
							<?php endif; ?>
						</p>
					</div>
					<section class="entry-content">
						<?php the_content(); ?>
					</section>

					<ul class="page_link cf">
						<li class="prev"><?php previous_post_link('%link', '« 前の記事へ', false); ?></li>
						<li class="next"><?php next_post_link('%link', '次の記事へ »', false); ?></li>
					</ul>

				</article>

				<?php endwhile; endif; ?>
				<?php wp_reset_query(); ?>
			</div>
			<!-- left_cont -->
		</div>
		<!-- wrapper -->
	</section>
</div>
<!-- contents -->

<?php get_footer(); ?>
