<?php get_header(); ?>

<div id="contents">

    <?php if(is_pc()): $posttype = "pctopslide"; else: $posttype = "sptopslide"; endif;　?>

    <?php
    $wp_query = new WP_Query();
    $param = array(
          'post_status' => 'publish',
          'order' => 'ASC',
          'post_type' => $posttype
    );
    $wp_query->query($param);?>


    <?php if(is_pc()): ?>

    <section class="mainimage">
        <div class="wideslider">
            <ul>
                <?php while($wp_query->have_posts()) :?>
                <?php $wp_query->the_post(); ?>
                <?php if(get_field('PC版_スライド画像１')): ?><li><a href="#1"><img src="<?php $image = get_field('PC版_スライド画像１'); echo $image['sizes']['medium']; ?>"></a></li><?php endif; ?>
                <?php if(get_field('PC版_スライド画像２')): ?><li><a href="#1"><img src="<?php $image = get_field('PC版_スライド画像２'); echo $image['sizes']['medium']; ?>"></a></li><?php endif; ?>
                <?php if(get_field('PC版_スライド画像３')): ?><li><a href="#1"><img src="<?php $image = get_field('PC版_スライド画像３'); echo $image['sizes']['medium']; ?>"></a></li><?php endif; ?>
                <?php if(get_field('PC版_スライド画像４')): ?><li><a href="#1"><img src="<?php $image = get_field('PC版_スライド画像４'); echo $image['sizes']['medium']; ?>"></a></li><?php endif; ?>
                <?php if(get_field('PC版_スライド画像５')): ?><li><a href="#1"><img src="<?php $image = get_field('PC版_スライド画像５'); echo $image['sizes']['medium']; ?>"></a></li><?php endif; ?>
                <?php if(get_field('PC版_スライド画像６')): ?><li><a href="#1"><img src="<?php $image = get_field('PC版_スライド画像６'); echo $image['sizes']['medium']; ?>"></a></li><?php endif; ?>
                <?php endwhile; ?>
            </ul>
        </div>
    </section>

    <?php else: ?>

    <div id="my-slider" class="slider-pro">
        <div class="sp-slides">
            <?php while($wp_query->have_posts()) :?>
            <?php $wp_query->the_post(); ?>
            <?php if(get_field('SP版_スライド画像１')): ?><div class="slider1 sp-slide"><img class="sp-image" src="<?php $image = get_field('SP版_スライド画像１'); echo $image['sizes']['medium']; ?>" /></div><?php endif; ?>
            <?php if(get_field('SP版_スライド画像２')): ?><div class="slider1 sp-slide"><img class="sp-image" src="<?php $image = get_field('SP版_スライド画像２'); echo $image['sizes']['medium']; ?>" /></div><?php endif; ?>
            <?php if(get_field('SP版_スライド画像３')): ?><div class="slider1 sp-slide"><img class="sp-image" src="<?php $image = get_field('SP版_スライド画像３'); echo $image['sizes']['medium']; ?>" /></div><?php endif; ?>
            <?php if(get_field('SP版_スライド画像４')): ?><div class="slider1 sp-slide"><img class="sp-image" src="<?php $image = get_field('SP版_スライド画像４'); echo $image['sizes']['medium']; ?>" /></div><?php endif; ?>
            <?php if(get_field('SP版_スライド画像５')): ?><div class="slider1 sp-slide"><img class="sp-image" src="<?php $image = get_field('SP版_スライド画像５'); echo $image['sizes']['medium']; ?>" /></div><?php endif; ?>
            <?php if(get_field('SP版_スライド画像６')): ?><div class="slider1 sp-slide"><img class="sp-image" src="<?php $image = get_field('SP版_スライド画像６'); echo $image['sizes']['medium']; ?>" /></div><?php endif; ?>
            <?php endwhile; ?>
        </div>
    </div>

    <?php endif; ?>


    <?php $args = array(
      'numberposts' => 5, //表示する記事の数
      'post_type' => 'camp' //投稿タイプ名
      // 条件を追加する場合はここに追記
    );
    $customPosts = get_posts($args);
    ?>

    <section class="camp">
        <div class="wrapper cf">
            <div class="left cf">
                <?php if($customPosts):?>
                <div class="top cf">
                    <img src="<?php bloginfo('template_url'); ?>/images/index_camp_icon.png">
                    <div class="text">
                        <p>お得な情報をゲット！</p>
                        <h3>最新キャンペーン情報</h3>
                    </div>
                    <!-- text -->
                    <?php endif; ?>
                </div><!-- cf -->
                <div class="recruit pc">
                    <a href="<?php bloginfo('url'); ?>/newgraduate_list/"><img src="<?php bloginfo('template_url'); ?>/images/index_camp_recruit_icon.svg" width="40" alt="">新卒採用情報はこちら</a>
                </div>
                <!--recruit -->
            </div>
            <div class="right">
                <?php if($customPosts):?>
                <?php if($customPosts) : foreach($customPosts as $post) : setup_postdata( $post ); ?>
                <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>">
                    <dl class="cf">
                        <dt><?php the_time('Y.m.d'); ?></dt>
                        <dd><?php echo $post->post_title;?></dd>
                    </dl>
                </a>
                <?php endforeach; ?>
                <?php endif;
                    wp_reset_postdata(); //クエリのリセット ?>
                <p class="linkbtn4"><a href="<?php bloginfo('url'); ?>/camplist/">一覧を見る</a></p>
                <div class="recruit sp">
                    <a href="<?php bloginfo('url'); ?>/newgraduate_list/"><img src="<?php bloginfo('template_url'); ?>/images/index_camp_recruit_icon.svg" alt="">新卒採用情報はこちら</a>
                </div>
                <!--recruit -->
                <?php endif; ?>
            </div>
        </div>
        <!-- wrapper -->
    </section>

    <!-- お知らせ -->
    <?php
    $wp_query = new WP_Query();
    $param = array(
      'posts_per_page' => '5', //表示件数。-1なら全件表示
      'post_status' => 'publish',
      'orderby' => 'date', //ID順に並び替え
      'order' => 'DESC'
    );
    $wp_query->query($param);?>
    <?php if($wp_query->have_posts()):?>

    <section class="news">
        <div class="wrapper">
            <h3>お知らせ</h3>

            <?php while($wp_query->have_posts()) :?>
            <?php $wp_query->the_post(); ?>
            <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>">
                <dl class="cf">
                    <dt>
                        <?php the_time('Y.m.d'); ?>
                    </dt>
                    <dd>
                        <?php echo $post->post_title;?>
                    </dd>
                </dl>
            </a>
            <?php endwhile; ?>
            <?php wp_reset_query(); ?>
            <p class="linkbtn4"><a href="<?php bloginfo('url'); ?>/newslist/">一覧を見る</a></p>
        </div>
        <!-- wrapper -->
    </section>
    <?php endif; ?>

    <section class="menu">
        <div class="wrapper">

            <ul class="cf">
                <li><a href="<?php bloginfo('url'); ?>/service/">
                        <div class="cf">
                            <img src="<?php bloginfo('template_url'); ?>/images/index_menu_photo1.jpg" alt="サービス | 札幌白洋舍">
                            <p>サービス</p>
                        </div>
                    </a></li>
                <li><a href="<?php bloginfo('url'); ?>/price/">
                        <div class="cf">
                            <img src="<?php bloginfo('template_url'); ?>/images/index_menu_photo2.jpg" alt="料金 | 札幌白洋舍">
                            <p>料金</p>
                        </div>
                    </a></li>
                <?php if(is_mobile()): ?>
            </ul>
            <ul class="cf">
                <?php endif; ?>
                <li><a href="<?php bloginfo('url'); ?>/shop/">
                        <div class="cf">
                            <img src="<?php bloginfo('template_url'); ?>/images/index_menu_photo3.jpg" alt="店舗情報 | 札幌白洋舍">
                            <p>店舗情報</p>
                        </div>
                    </a></li>
                <li><a href="<?php bloginfo('url'); ?>/delivery/">
                        <div class="cf">
                            <img src="<?php bloginfo('template_url'); ?>/images/index_menu_photo4.jpg" alt="集配サービス | 札幌白洋舍">
                            <p>集配サービス</p>
                        </div>
                    </a></li>
            </ul>
        </div>
        <!-- wrapper -->
    </section>

    <section class="recruit cf">
        <div class="left">
        </div>
        <!-- left -->
        <div class="right">
            <p class="en">RECRUIT</p>
            <h3>採用情報</h3>
            <p>札幌白洋舍では一緒に働く仲間を募集しています</p>
            <p class="linkbtn5"><a href="<?php bloginfo('url'); ?>/recruit_list">詳細はこちら</a></p>
        </div>
        <!-- right -->
    </section>
</div>
<!--contents -->
<?php get_footer(); ?>
