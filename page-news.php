<?php
/*
Template Name: page-newslist
*/
?>
<?php get_header(); ?>
<?php include (TEMPLATEPATH . '/part-title.php'); ?>

<div id="contents">
  <?php include (TEMPLATEPATH . '/part-pan.php'); ?>
  <h2 class="headline03">お知らせ</h2>
  <?php
				$paged = (int) get_query_var('paged');
		$wp_query = new WP_Query();
		$param = array(
			'post_status' => 'publish',
			'orderby' => 'date',
			'paged' => $paged,
			'order' => 'DESC'
		);
		$wp_query->query($param);?>
  <?php if ( have_posts() ) :?>
  <section class="newslist">
    <?php while ( have_posts() ) : the_post(); ?>
    <dl class="cf">
      <dt>
        <?php the_time('Y年m月d日'); ?>
        <br></dt>
      <dd><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>">
        <h3>
          <?php if(mb_strlen($post->post_title)>27) { $title= mb_substr($post->post_title,0,27) ; echo $title. '...' ;
} else {echo $post->post_title;}?>

        </h3>
        <p> <?php echo get_the_custom_excerpt( get_the_content(), 90 ) ?></p>
        </a></dd>
    </dl>
    <?php endwhile; ?>
  </section>
  <div class="pagination"> <?php echo bmPageNaviGallery(); // ページネーション出力 ?> </div>
  <!-- pagination -->
  <?php else : ?>
  記事が見つかりません。
  <?php endif; ?>
  <?php wp_reset_query(); ?>
</div>
<!-- contents -->

<?php get_footer(); ?>
