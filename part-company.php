<section class="menu">
  <div class="wrapper">
  <ul class="cf">
    <li><a href="<?php bloginfo('url'); ?>/company/"><img src="<?php bloginfo('template_url'); ?>/images/company_menu01.jpg" alt="会社概要 | 札幌白洋舍">
      <h3>会社概要</h3>
      </a></li>
    <li><a href="<?php bloginfo('url'); ?>/company#history"><img src="<?php bloginfo('template_url'); ?>/images/company_menu02.jpg" alt="沿革 | 札幌白洋舍">
      <h3>沿革</h3>
      </a></li>
    <li><a href="<?php bloginfo('url'); ?>/business/"><img src="<?php bloginfo('template_url'); ?>/images/company_menu03.jpg" alt="事業案内 | 札幌白洋舍">
      <h3>事業案内</h3>
      </a></li>
  </ul>
  <!--wrapper --> 
</section>
<section class="company">
  <div class="wrapper">
    <h3 class="headline01">会社概要</h3>
    <table class="style01">
      <tbody>
        <tr>
          <th>商号</th>
          <td>札幌白洋舍株式会社</td>
        </tr>
        <tr>
          <th>事業内容</th>
          <td>クリーニング業</td>
        </tr>
        <tr>
          <th>設立</th>
          <td>平成２０年１月</td>
        </tr>
        <tr>
          <th>資本金</th>
          <td>9500万</td>
        </tr>
        <tr>
          <th>従業員数</th>
          <td>120人</td>
        </tr>
        <tr>
          <th>電話</th>
          <td>011-631-2306</td>
        </tr>
        <tr>
          <th>所在地</th>
          <td>〒063-0006 札幌市西区山の手６条１丁目３－３４</td>
        </tr>

      </tbody>
    </table>
          <div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2450.680226653448!2d141.29306658551192!3d43.075792518486566!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5f0b2837dce9f18d%3A0x8dc79bac4ce5e57b!2z44CSMDYzLTAwMDYg5YyX5rW36YGT5pyt5bmM5biC6KW_5Yy65bGx44Gu5omL77yW5p2h77yR5LiB55uu77yT4oiS77yT77yU!5e0!3m2!1sja!2sjp!4v1510328148666" frameborder="0" style="border:0" allowfullscreen></iframe></div>
  </div>
  <!-- wrapper --> 
  

</section>


 <section class="history" id="history">
  <div class="wrapper">
    <h3 class="headline01">沿革</h3>
    <table class="style01">
      <tbody>
      <tr>
          <th>昭和７年５月</th>
          <td>札幌支店開設</td>
        </tr>
        <tr>
          <th>平成２０年１月</th>
          <td>札幌支店から独立し、札幌白洋舍株式会社を設立</td>
        </tr>
      </tbody>
    </table>
  </div>
  <!-- wrapper --> 
</section>
