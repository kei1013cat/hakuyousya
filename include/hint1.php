<div class="modal-options1" data-izimodal-loop="" data-izimodal-title="ローヤル料金">
<p>より汚れ落ちのよい独自の洗浄システム、細部にわたる手仕上げといったハイグレードなクリーニング品質。さらに、しみ残りやほつれ、ボタンの緩みなどを丁寧に点検した上で、必要に応じてしみ抜き、補修も行うメンテナンス付クリーニングです。衣替えの時などに、ワードローブの定期点検としてご利用いただけば、大切な一着を常にベストな状態にキープできます。</p>
<p><a href="<?php bloginfo('url'); ?>/service_course#loyal" target="_blank">詳細はこちら</a></p>
</div>