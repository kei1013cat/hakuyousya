<div class="modal-options2" data-izimodal-loop="" data-izimodal-title="高級ブランド・素材別料金">
  <p>高級ブランド品のシンボルとも言えるロゴ、マーク、ネーム、タグなどを大切に丁寧に取り扱います。ファスナーやボタン、バックルなど付属品が傷ついたり、外れたりすることがないようカバーで保護します。取れかけたボタンや、裾などの簡単なほつれは無料で修理します。</p>
  <p><a href="<?php bloginfo('url'); ?>/service_course#bland" target="_blank">詳細はこちら</a></p>
</div>
