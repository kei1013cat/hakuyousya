<div class="modal-options3" data-izimodal-loop="" data-izimodal-title="クリスタル料金">
  <p>クリスタルクリーニングはハイグレードな水洗いクリーニングです。ローヤルクリーニングと同様に細部の手仕上げ、しみ抜き・補修のメンテナンスも行います。
素材やデザインに合わせ多様な洗浄コースを用意し、通常は水で洗いにくいものでもご要望や必要に応じて幅広くお取り扱いいたします。</p>
<p><a href="<?php bloginfo('url'); ?>/service_course#loyal" target="_blank">詳細はこちら</a></p>
</div>
