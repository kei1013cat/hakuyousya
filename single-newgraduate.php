<?php get_header(); ?>
<div id="page_recruit">
<?php include (TEMPLATEPATH . '/part-title.php'); ?>

<div id="contents">
<?php include (TEMPLATEPATH . '/part-pan.php'); ?>


    <?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
<section id="page_recruit">
    <section class="recruit_info">
      <div class="wrapper">

        <h3 class="headline01"><?php the_title(); ?></h3>
        <table class="style01">
          <tbody>
            
            <?php if(get_field('職種')): ?>
            <tr>
              <th>職種</th>
              <td><?php echo get_field('職種'); ?><br />(<?php echo get_field('従業員種別'); ?>)</td>
            </tr>
            <?php endif; ?>
            
            <?php if(get_field('勤務地')): ?>
            <tr>
              <th>勤務先</th>
              <td><?php echo get_field('勤務地'); ?><?php if(get_field('GoogleMap')): ?><br>
                <a href="<?php echo get_field('GoogleMap'); ?>" target="_blank">地図はこちら</a><?php endif; ?></td>
            </tr>
            <?php endif; ?>
            
            <?php if(get_field('給与')): ?>
            <tr>
              <th>給与</th>
              <td><?php echo get_field('給与'); ?></td>
            </tr>
            <?php endif; ?>
            
            
            <?php if(get_field('勤務曜日・時間')): ?>
            <tr>
              <th>勤務曜日・時間</th>
              <td><?php echo get_field('勤務曜日・時間'); ?></td>
            </tr>
            <?php endif; ?>
            
            <?php if(get_field('資格')): ?>
            <tr>
              <th>資格</th>
              <td><?php echo get_field('資格'); ?></td>
            </tr>
            <?php endif; ?>
            
            <?php if(get_field('休日')): ?>
            <tr>
              <th>休日</th>
              <td><?php echo get_field('休日'); ?></td>
            </tr>
            <?php endif; ?>
            
            <?php if(get_field('待遇')): ?>
            <tr>
              <th>待遇</th>
              <td><?php echo get_field('待遇'); ?></td>
            </tr>
            <?php endif; ?>
            
            <?php if(get_field('ポイント')): ?>
            <tr>
              <th>ポイント</th>
              <td><?php echo get_field('ポイント'); ?></td>
            </tr>
            <?php endif; ?>
            </tbody>
        </table>
    		<p class="linkbtn2"><a href="<?php bloginfo('url'); ?>/recruit_form/?str=<?php the_title(); ?>">この求人に応募する</a></p>
      </div>
      <!-- wrapper -->
    </section>



    <?php endwhile; ?>
    <?php endif; ?>
    <?php wp_reset_query(); ?>

</section>


</div>
<!-- contents -->
</div>
<?php get_footer(); ?>
