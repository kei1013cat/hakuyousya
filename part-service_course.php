<section id="page_service">
    <section class="service">
      <div class="wrapper">
      <h3 class="headline02"><span>オリジナルクリーニング</span></h3>
        <dl class="cf">
          <dt class="pc"><img src="<?php bloginfo('template_url'); ?>/images/service_course_dry.jpg?v=20180130" alt="ドライクリーニング | 札幌白洋舍"></dt>
          <dd>
            <h4 class="icon1">ドライクリーニング<span class="price">オリジナル料金</span></h4>
            <p class="sp"><img src="<?php bloginfo('template_url'); ?>/images/service_course_dry.jpg"></p>
            <p>白洋舍は創業翌年の1907年、独自開発により日本で初めてのドライクリーニングを導入しました。ドライクリーニングの利点は生地へのダメージや色落ちの少ない優しい洗い。デリケートな素材でも色落ちや型崩れ、風合いの変化を防ぎきれいに洗い上げることができます。 白洋舍のドライクリーニングは、素材やデザインに合わせ複数のドライクリーニング洗浄液を使い分け、最適なクリーニングを実現、丁寧な仕上げで着心地の回復にもこだわっています。</p>
          </dd>
        </dl>
        <dl class="cf">
          <dt class="pc"><img src="<?php bloginfo('template_url'); ?>/images/service_course_wet.jpg" alt="ランドリー（水洗い） | 札幌白洋舍"></dt>
          <dd>
            <h4 class="icon1">ランドリー（水洗い）<span class="price">オリジナル料金</span></h4>
            <p class="sp"><img src="<?php bloginfo('template_url'); ?>/images/service_course_wet.jpg"></p>
            <p>ポロシャツやスポーツウェア、ブラウスや夏物のパンツなど肌に直接触れて汗や皮脂で汚れるアイテムには水洗いが最適です。白洋舍のランドリー（水洗い）は素材やデザインに合わせ適切な水温で、独自の洗剤を使って洗い上げます。ご家庭での洗濯とは一味違う汚れ落ちを実感していただけます。 また、プロの丁寧な仕上げで、仕上り品にもきっとご満足いただけるはずです。</p>
          </dd>
        </dl>
</div>
      <!-- wrapper --> 
    </section>
    
    
    <section class="service_col2 blue cf" id="loyal">
      <h3 class="headline02"><span>高品質クリーニング</span></h3>
      <div class="wrapper">
        <dl class="cf">
          <dt><h4 class="icon1">ローヤルクリーニング<span class="price">ローヤル料金</span></h4><img src="<?php bloginfo('template_url'); ?>/images/service_course_royal.jpg" alt="ローヤルクリーニング | 札幌白洋舍"></dt>
          <dd>
            <p>洋服の素材の持つ柔らかさなどの風合いや光沢、鮮やかな色合いを守ることにもこだわった独自の洗浄システムを採用しています。繰り返しの着用によって気づかないうちにくすんでいた色も、きれいに洗い上げることでより鮮やかによみがえります。</p>
          </dd>
        </dl>
        <dl class="cf">
          <dt><h4 class="icon1">クリスタルクリーニング<span class="price">クリスタル料金</span></h4><img src="<?php bloginfo('template_url'); ?>/images/service_course_cry.jpg" alt="クリスタルクリーニング | 札幌白洋舍"></dt>
          <dd>
            <p>肌に直接触れる夏物衣料などにつきやすい汗などの水溶性の汚れは、ドライクリーニングよりも水洗いの方がすっきりと落すことができます。クリスタルクリーニングなら、素材に合わせた多様な洗浄プログラムの開発により、汚れと素材に合った最適な方法で、洋服へのダメージを最小限に抑えながら水洗いができます。また、夏の日差しによる洋服の色褪せを防ぐＵＶカット加工を全品に施します。</p>
          </dd>
        </dl>
      </div>
      <!-- wrapper --> 
    </section>


    <section class="loyal_description">
      <div class="wrapper">

    	<dl class="cf">
    		<dt class="pc">
	    		<img src="<?php bloginfo('template_url'); ?>/images/service_course_detail1.jpg" alt="仕上げ | 札幌白洋舍">
        	</dt>
        	<dd>
            
        		<h3>仕上げ</h3>
        		<p>着用時のシルエットを考え立体的なラインの復元にこだわって細部にまで手仕上げを加えていく、ちょっと上をいく上質仕上げです。技術者はファッションの流行にも気を配り新しいデザインのものでも、買ったときの形に近づけることを常に念頭に知識と技術を磨いています。</p>
        	</dd>
    	</dl>
    	<dl class="cf">
    		<dt class="pc">
	    		<img src="<?php bloginfo('template_url'); ?>/images/service_course_detail2.jpg" alt="メンテナンス | 札幌白洋舍">
        	</dt>
        	<dd>
        		<h3>メンテナンス</h3>
        		<p>ローヤル/クリスタルクリーニングでは、受付時や、クリーニングの過程、仕上がり時に行うチェックの際にしみやほつれ、ボタンのゆるみなどを徹底点検。しみや不具合がみつかった場合はしみ抜き、補修を行います。<span class="gray">※一定範囲以上の大きなしみ、ほつれ等の場合は別途料金を申し受けます。</span>
汚れを落とししわを伸ばすクリーニングの基本性能もちょっと上を行く上質、さらに、お客さまの大事な一着を常にベストな状態に保つためのメンテナンスが加わり、大切なワードローブの定期点検として衣替えなどの機会にご利用いただけます。</p>
        	</dd>
    	</dl>
    	<dl class="cf">
    		<dt class="pc">
	    		<img src="<?php bloginfo('template_url'); ?>/images/service_course_detail3.jpg" alt="包装 | 札幌白洋舍">
        	</dt>
        	<dd>
        		<h3>包装</h3>
        		<p>ローヤル/クリスタルクリーニングでは仕上がり品のお返しに当たり、立体的な仕上がりをキープするために、厚みのある上級仕様のハンガーを使用しています。また、ハンガーにはM・Lの二つのサイズが用意され、洋服のサイズに合わせて使い分けることにより、仕上がり後の型崩れを防止しています。</p>
        	</dd>
    	</dl>
	  </div>
    </section>
    <!-- description -->
    
    
     <section class="service bland" id="bland">
      <div class="wrapper">
      <h3 class="headline02"><span>高級ブランドクリーニング</span></h3>
        <dl class="cf top">
          <dt class="pc"><img src="<?php bloginfo('template_url'); ?>/images/service_course_bland.jpg" alt="高級ブランドクリーニング | 札幌白洋舍"></dt>
          <dd>
            <h4 class="icon1">高級ブランドクリーニング<span class="price">高級ブランド・素材別料金</span></h4>
            <p class="sp"><img src="<?php bloginfo('template_url'); ?>/images/service_course_bland.jpg" alt="高級ブランドクリーニング | 札幌白洋舍"></p>
            <p>高級ブランド品のシンボルとも言えるロゴ、マーク、ネーム、タグなどを大切に丁寧に取り扱います。ファスナーやボタン、バックルなど付属品が傷ついたり、外れたりすることがないようカバーで保護します。取れかけたボタンや、裾などの簡単なほつれは無料で修理します 高級ブランド品を末長くご愛用いただくために、よりきれいに洗い上げる洗浄システムを採用しています。汚れやしみを落としてきれいにすることはもちろん、素材やファッションに合わせた洗浄液や洗浄方法を選び、しみはしみ抜き技術者が入念にしみ抜きをしてきれいにします。</p>
          </dd>
        </dl>
		<div class="middle">
			<ul class="cf">
				<li>
					<h3>"ブランド"へのこだわり</h3>
					<img src="<?php bloginfo('template_url'); ?>/images/service_about_brand1.jpg" alt="ブランドへのこだわり１ | 札幌白洋舍">
					<img src="<?php bloginfo('template_url'); ?>/images/service_about_brand2.jpg" alt="ブランドへのこだわり２ | 札幌白洋舍">
					<img src="<?php bloginfo('template_url'); ?>/images/service_about_brand3.jpg" alt="ブランドへのこだわり３ | 札幌白洋舍">
					<p>高級ブランド品のシンボルとも言えるロゴ、マーク、ネーム、タグなどを大切に丁寧に取り扱います。ファスナーやボタン、バックルなど付属品が傷ついたり、外れたりすることがないようカバーで保護します。取れかけたボタンや、裾などの簡単なほつれは無料でします。</p>
				</li>
				<li>
					<h3>"きれい・清潔"へのこだわり</h3>
					<img src="<?php bloginfo('template_url'); ?>/images/service_about_clean1.jpg" alt="きれい・清潔へのこだわり１ | 札幌白洋舍">
					<img src="<?php bloginfo('template_url'); ?>/images/service_about_clean2.jpg" alt="きれい・清潔へのこだわり２ | 札幌白洋舍">
					<img src="<?php bloginfo('template_url'); ?>/images/service_about_clean3.jpg" alt="きれい・清潔へのこだわり３ | 札幌白洋舍">
					<p>高級ブランド品を末長くご愛用いただくために、よりきれいに洗い上げる洗浄システムを採用しています。汚れやしみを落としてきれいにすることはもちろん、素材やファッションに合わせた洗浄液や洗浄方法を選び、しみはしみ抜き技術者が入念にしみ抜きをしてきれいにします。</p>
				</li>
			</ul>
			<ul class="cf">
				<li>
					<h3>"風合い"へのこだわり</h3>
					<img src="<?php bloginfo('template_url'); ?>/images/service_about_handling1.jpg" alt="風合いへのこだわり１ | 札幌白洋舍">
					<img src="<?php bloginfo('template_url'); ?>/images/service_about_handling2.jpg" alt="風合いへのこだわり２ | 札幌白洋舍">

					<p>摩擦によって毛羽立ちや毛並み、風合いが変わりやすい素材や、ボタンやスパンコール、ビジューなどで装飾されている品物などは、洗浄はもちろん、乾燥方法にも配慮してデリケートに取り扱います。</p>
				</li>
				<li>
					<h3>"デザイン"へのこだわり</h3>
					<img src="<?php bloginfo('template_url'); ?>/images/service_about_design1.jpg"　alt="デザインへのこだわり１ | 札幌白洋舍">
					<img src="<?php bloginfo('template_url'); ?>/images/service_about_design2.jpg"　alt="デザインへのこだわり２ | 札幌白洋舍">
					<img src="<?php bloginfo('template_url'); ?>/images/service_about_design3.jpg"　alt="デザインへのこだわり３ | 札幌白洋舍">
					<p>デザインや素材に合わせて細部まで手仕上げを加えて丁寧に立体的に仕上げます。また、仕上がった品物が形くずれしないよう、厚みのあるハンガーや薄紙を使用することによりデザインや生地を保護します。</p>
				</li>
			</ul>
			<ul class="cf">
				<li>
					<h3>"品質管理"へのこだわり</h3>
					<img src="<?php bloginfo('template_url'); ?>/images/service_about_quality1.jpg"　alt="品質管理へのこだわり１ | 札幌白洋舍">
					<img src="<?php bloginfo('template_url'); ?>/images/service_about_quality2.jpg"　alt="品質管理へのこだわり２ | 札幌白洋舍">
					<img src="<?php bloginfo('template_url'); ?>/images/service_about_quality3.jpg"　alt="品質管理へのこだわり３ | 札幌白洋舍">
					<p>常に一歩先を見据え、洗剤やドライクリーニング溶剤、各種加工剤、クリーニング方法などを研究・開発しています。汚れ落ち、製品へのダメージ、風合いを考慮しながら、環境にも配慮した新しいクリーニング方法を構築しています。</p>
				</li>
				<li></li>
			</ul>
		</div>
		<!-- middle -->

        <dl class="cf bottom">
          <dt class="pc"><img src="<?php bloginfo('template_url'); ?>/images/service_course_sozai.jpg"　alt="素材別クリーニング | 札幌白洋舍"></dt>
          <dd>
            <h4 class="icon1">素材別クリーニング<span class="price">高級ブランド・素材別料金</span></h4>
            <p class="sp"><img src="<?php bloginfo('template_url'); ?>/images/service_course_sozai.jpg"　alt="素材別クリーニング | 札幌白洋舍"></p>
            <p class="text">繊細さ、柔らかさ、滑らかさ、光沢など独自の魅力を持ち、特別なケアが求められる上質素材のためのクリーニングです。素材に合わせた最適なクリーニングに加え、ローヤルクリーニング同様の手仕上げやしみ抜き・補修のメンテナンスも魅力です。<br>・カシミヤ・アンゴラ・アルパカ・モヘヤ・ラマ・キャメル・ビキューナなどの獣毛製品・シルク・ベルベット・麻などの独特の風合いを持った製品などに</p>
          </dd>
        </dl>
        </div>
        </section>
</section>