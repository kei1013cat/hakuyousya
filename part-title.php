<?php
    $parent_id = $post->post_parent; // 親ページのIDを取得
    $parent_slug = get_post($parent_id)->post_name; // 親ページのスラッグを取得
?>
<div id="pagetitle" >
	<div class="wrapper"><h2>
	<?php if(is_single() && $post->post_type =="post"):?>
		お知らせ
	<?php elseif((is_single() || is_archive()) && $post->post_type =="camp"):?>
		キャンペーン情報
	<?php elseif((is_single() || is_archive()) && $post->post_type =="recruit"):?>
		募集要項
	<?php else: ?>
		<?php echo get_post($post->post_parent)->post_title;?>
	<?php endif; ?>
	<span>
	<?php 

	if($post->post_name =="service"):
		echo "SERVICE";
	elseif($post->post_name =="company" || $post->post_name =="business"):
		echo "COMPANY";
	elseif($post->post_name =="price"):
		echo "PRICE";
	elseif($post->post_name =="shop"):
		echo "SHOP";
	elseif($post->post_name =="delivery"):
		echo "DELIVERY";
	elseif($post->post_name =="recruit_list"):
		echo "RECRUIT";
	elseif($post->post_name =="inquiry" || $parent_slug == "inquiry"):
		echo "INQUIRY";
	elseif($post->post_name =="service_course"):
		echo "COURSE";
	elseif($post->post_name =="recruit_form" || $parent_slug == "recruit_form"):
		echo "RECRUIT　FORM";
	elseif($post->post_name =="recruit"):
		echo "RECRUIT";
	elseif($post->post_name =="delivery_form" || $parent_slug=="delivery_form" ):
		echo "DELIVERY　FORM";
	elseif(is_single() && $post->post_type =="post"):
		echo "NEWS";
	elseif(((is_single() || is_archive()) && $post->post_type =="camp" ) || $post->post_name =="camplist"):
		echo "CAMPAIGN";
	elseif((is_single() || is_archive()) && $post->post_type =="recruit"):
		echo "RECRUIT";
	elseif($post->post_name =="newslist"):
		echo "What's New";
	endif;
	?>
	</span>
	</h2>

	<?php if($post->post_name =="price"): ?>
        <p>2018年6月1日 改定<br>
        本料金表は札幌地区料金表です</p>

	<?php endif; ?>

</div>
	<!-- wrapper -->
	
</div>
<!-- page_title -->