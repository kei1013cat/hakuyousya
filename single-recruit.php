<?php get_header(); ?>
<div id="page_recruit">
<?php include (TEMPLATEPATH . '/part-title.php'); ?>

<div id="contents">
<?php include (TEMPLATEPATH . '/part-pan.php'); ?>


    <?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
<section id="page_recruit">
    <section class="recruit_info">
      <div class="wrapper">

        <h3 class="headline01"><?php the_title(); ?></h3>
        <table class="style01">
          <tbody>
            <?php $kinmusaki = get_field('勤務先');?>
            <tr>
              <th>勤務地</th>
              <td>
                <?php if($kinmusaki): ?><?php echo get_field('勤務先'); ?><br><?php endif; ?>
                <?php echo get_field('勤務地'); ?><br>
                <a href="<?php echo get_field('GoogleMap'); ?>" target="_blank">地図はこちら</a></td>
            </tr>
            <tr>
              <th>勤務曜日・時間</th>
              <td><?php echo get_field('勤務曜日・時間'); ?></td>
            </tr>
            <tr>
              <th>資格</th>
              <td><?php echo get_field('資格'); ?></td>
            </tr>
            <tr>
              <th>休日</th>
              <td><?php echo get_field('休日'); ?></td>
            </tr>
            <tr>
              <th>待遇</th>
              <td><?php echo get_field('待遇'); ?></td>
            </tr>
            <?php $point = get_field('ポイント');?>
            <?php if($point): ?>
            <tr>
              <th>ポイント</th>
              <td><?php echo get_field('ポイント'); ?></td>
            </tr>
            <?php endif; ?>
            </tbody>
        </table>
    		<p class="linkbtn2"><a href="<?php bloginfo('url'); ?>/recruit_form/?str=<?php the_title(); ?>">この求人に応募する</a></p>
      </div>
      <!-- wrapper -->
    </section>



    <?php endwhile; ?>
    <?php endif; ?>
    <?php wp_reset_query(); ?>

</section>


</div>
<!-- contents -->
</div>
<?php get_footer(); ?>
