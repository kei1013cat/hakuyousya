<section id="page_recruit">
    <section class="recruit_info">
      <div class="wrapper">
       
        <h3 class="headline01">募集要項</h3>
        <table class="style01">
          <tbody>
            <tr>
              <th>勤務地</th>
              <td>アピア店</td>
            </tr>
            <tr>
              <th>勤務曜日・時間</th>
              <td> 9:00～22:00</td>
            </tr>
            <tr>
              <th>資格</th>
              <td> 未経験者大歓迎です！ フリーター、学生、主婦活躍中！ ・夕方以降と土日だけ働きたい学生さん ・日中の8時間を有効に使いたい ・短時間勤務を希望の方 あなたの希望、ご相談ください！ 幅広い年齢層のスタッフが活躍中です♪ まずはお気軽にご応募ください！</td>
            </tr>
            <tr>
              <th>休日</th>
              <td> 店舗にお問い合わせください</td>
            </tr>
            <tr>
              <th>待遇</th>
              <td>交通費支給(規定あり)<br>昇給あり<br>年2回の賞与あり<br>有給休暇あり<br>社会保険完備<br>従業員割引あり<br>制服貸与<br>社内資格制度あり(資格取得者は時給UP！)<br>社員登用あり</td>
            </tr>
            <tr>
              <th>ポイント</th>
              <td>■働きやすいシフト制♪<br>　シフトはご相談いただけます。<br>　ダブルワークや、大学生の学校帰り、<br>　主婦の家事の合間など、あなたのライフ<br>　スタイルに合わせてお仕事できます！<br>■未経験でも大丈夫！<br>　前職経験は問いません。<br>■働きやすい職場です♪<br>　長期でお勤めのスタッフも多数<br>　いらっしゃいます。<br>まずはお気軽にご応募ください♪</td>
            </tr>
            </tbody>
        </table>
<p class="linkbtn2"><a href="<?php bloginfo('url'); ?>/recruit_form/">この求人に応募する</a></p>
      </div>
      <!-- wrapper --> 
    </section>
</section>