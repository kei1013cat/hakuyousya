<section class="topmenu">
  <div class="wrapper">
    <div class="tac"><h3 class="headline02"><span>クリーニングサービス</span></h3></div>
    <p class="text">1906年の創業から百余年、人々の生活スタイルの変化に合わせ、白洋舍のクリーニングサービスは昔も今も進化と発展を続けています。きものや洋服に始まったクリーニングは靴・バッグやインテリアのクリーニングなど幅を広げ、便利さを求めるお客さまの声を受けて、保管サービスや宅配便によるクリーニングの取り扱いなどサービスの内容も多様化してきました。あんなものも、こんなことも。お客さまの要望に応える白洋舍のさまざまなサービスをご案内します。</p>
    
    <div class="menu">
      <ul class="cf">
        <li><a href="#a1">衣類のクリーニング</a></li>
        <li><a href="#a2">クリーニング関連サービス</a></li>
        <li><a href="#a3">集配サービス</a></li>
        <li><a href="#a4">ハウスクリーニング・リフォーム</a></li>
      </ul>
    </div>
    <!--menu --> 
  </div>
  <!-- wrapper --> 
</section>
<section class="service">
  <div class="wrapper">
    <dl class="cf" id="a1">
      <dt class="pc"><img src="<?php bloginfo('template_url'); ?>/images/service_photo01.jpg" alt="衣類のクリーニング | 札幌白洋舍"></dt>
      <dd>
        <h4 class="icon1">衣類のクリーニング<!--<a href="#">詳細はこちら →</a>--></h4>
        <p class="sp"><img src="<?php bloginfo('template_url'); ?>/images/service_photo01.jpg" alt="衣類のクリーニング | 札幌白洋舍"></p>
        <p>白洋舍は、日本のファッションの変遷をずっと見守っています。デザインや素材は流行でめまぐるしく変遷しますが、「お気に入りのワードローブをもっときれいに、もっとスタイリッシュに着たい」という、お客さまの気持ちはいつの日も変わりません。そんな気持ちに応えるために。<br>白洋舍の上質クリーニングをあなたの「身にまとう喜び」のために役立ててください。</p>
        <p class="linkbtn3"><a href="<?php bloginfo('url'); ?>/service_course/">クリーニングのコースはこちら</a></p>

        <ul class="otherlink">
          <li><a href="http://www.hakuyosha.co.jp/cleaning/clothes/shirt/" target="_blank">・白洋舍のワイシャツクリーニング</a></li>
          <li><a href="http://www.hakuyosha.co.jp/cleaning/clothes/option/" target="_blank">・機能をプラス---オプションサービス</a></li>
          <li><a href="http://www.hakuyosha.co.jp/cleaning/clothes/special/" target="_blank">・きもの・ファー・レザーのクリーニング</a></li>
          <li><a href="http://www.hakuyosha.co.jp/cleaning/clothes/accessory/" target="_blank">・靴・バッグetc…こんなものもきれいにできます</a></li>
          <li><a href="http://www.hakuyosha.co.jp/cleaning/clothes/custom/" target="_blank">・かけがえのない一着に。カスタムクリーニング</a></li>
        </ul>
      </dd>
    </dl>
    <dl class="cf" id="a2">
      <dt class="pc"><img src="<?php bloginfo('template_url'); ?>/images/service_photo02.jpg" alt="クリーニング関連サービス | 札幌白洋舍"></dt>
	       <dd>
        <h4 class="icon2">クリーニング関連サービス</h4>
        <p class="sp"><img src="<?php bloginfo('template_url'); ?>/images/service_photo02.jpg" alt="クリーニング関連サービス | 札幌白洋舍"></p>
        <p>白洋舍のクリーニングの魅力は洋服や身の回り品をきれいにするだけではありません。便利さやお得を求めるお客さまの声にお応えし、夏の間冬物衣料などをお預かりする保管サービスをはじめ、さまざまなサービスを提供しています。</p>
        <ul class="otherlink">
		<li><a href="http://www.hakuyosha.co.jp/cleaning/service/#01" target="_blank">・便利な保管サービス</a></li>
		<li><a href="http://www.hakuyosha.co.jp/cleaning/service/#02" target="_blank">・クイックサービス</a></li>
		<li><a href="http://www.hakuyosha.co.jp/cleaning/service/#03" target="_blank">・うららか会員サービス</a></li>
		<li><a href="http://www.hakuyosha.co.jp/cleaning/service/#04" target="_blank">・回数券</a></li>
		<li><a href="http://www.hakuyosha.co.jp/cleaning/service/#05" target="_blank">・販売商品</a></li>
        </ul>
      </dd>
    </dl>
    <dl class="cf" id="a3">
      <dt class="pc"><img src="<?php bloginfo('template_url'); ?>/images/service_photo03.jpg" alt="集配サービス | 札幌白洋舍"></dt>
      <dd>
        <h4 class="icon3">集配サービス</h4>
        <p class="sp"><img src="<?php bloginfo('template_url'); ?>/images/service_photo03.jpg" alt="集配サービス | 札幌白洋舍"></p>
        <p>衣類のケアに関する知識を持つ白洋舍の集配担当者がお客さまのお宅を訪問し、クリーニング品のお預かり、お届けを行うサービスです。定期的な訪問サービスのほか、必要に応じて電話やネットからお申込みいただくこともできます。<br>白洋舍のクリーニングは創業当初、集配サービスから始まりました。百余年の伝統のある古くて新しいサービスです。</p>
        <p class="linkbtn3"><a href="<?php bloginfo('url'); ?>/delivery/">集配サービスの詳細はこちら</a></p>
      </dd>
    </dl>
   
    <dl class="cf" id="a4">
      <dt class="pc"><img src="<?php bloginfo('template_url'); ?>/images/service_photo04.jpg" alt="ハウスクリーニング・リフォーム | 札幌白洋舍"></dt>
      <dd>
        <h4 class="icon5">ハウスクリーニング・リフォーム</h4>
        <p class="sp"><img src="<?php bloginfo('template_url'); ?>/images/service_photo04.jpg" alt="ハウスクリーニング・リフォーム | 札幌白洋舍"></p>
        <p>健康な暮らしに欠かせないカーテン・じゅうたん・ふとんのクリーニングは、白洋舍のスタッフがご自宅まで取りに伺う集配サービスも利用できるほか、オプションサービスも充実です。また、キッチン周りやバスルーム、トイレから家まるごとのハウスクリーニングや、エアコン、ソファーなどのクリーニングも。住まいのあれもこれもきれいにできます。</p>
        <ul class="otherlink">
		<li><a href="http://www.hakuyosha.co.jp/cleaning/house/curtain/" target="_blank">・カーテン</a></li>
		<li><a href="http://www.hakuyosha.co.jp/cleaning/house/carpet/" target="_blank">・じゅうたん</a></li>
		<li><a href="http://www.hakuyosha.co.jp/cleaning/house/futon/" target="_blank">・ふとん</a></li>
		<li><a href="http://www.hakuyosha.co.jp/cleaning/house/other/" target="_blank">・その他</a></li>
    </ul>
      </dd>
    </dl>
  </div>
  <!-- wrapper --> 
</section>
