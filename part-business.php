
<section id="page_company">
<section class="menu">
  <div class="wrapper">
  <ul class="cf">
    <li><a href="<?php bloginfo('url'); ?>/company/"><img src="<?php bloginfo('template_url'); ?>/images/company_menu01.jpg" alt="会社概要">
      <h3>会社概要</h3>
      </a></li>
    <li><a href="<?php bloginfo('url'); ?>/company#history"><img src="<?php bloginfo('template_url'); ?>/images/company_menu02.jpg" alt="沿革">
      <h3>沿革</h3>
      </a></li>
    <li><a href="<?php bloginfo('url'); ?>/business/"><img src="<?php bloginfo('template_url'); ?>/images/company_menu03.jpg" alt="事業案内">
      <h3>事業案内</h3>
      </a></li>
  </ul>
  <!--wrapper --> 
</section>

<section class="business">
<div class="wrapper">
<h3 class="headline01">事業案内</h3>
<dl class="cf">
<dt class="pc"><img src="<?php bloginfo('template_url'); ?>/images/company_business_photo01.jpg" alt="クリーニング事業"></dt>
<dd><h4>クリーニング事業</h4>
<p class="sp"><img src="<?php bloginfo('template_url'); ?>/images/company_business_photo01.jpg" alt="クリーニング事業"></p>
  <p>当社は1906年の創業翌年、独自開発により日本に初めてとなるドライクリーニングを導入、以来、百余年に渡り、業界のリーディングカンパニーとして、日本のクリーニング業の発展に寄与して参りました。<br>国内に15、海外に2つの個人向けの営業拠点を展開し、店舗網及び集配ルート網によりサービスを提供、近年は宅配便による取り扱いにも力を入れています。扱い品目も一般の衣料品から扱いの難しいきものやファー、レザー、さらには靴、バッグなどの身の回り品やカーテン、じゅうたん、ふとんなどインテリア関連、キッチン、バスルームなどのハウスクリーニングと多岐にわたり、そのすべてにお客さまに満足いただける高品質を追求して、研究開発、技術、技能の向上に取り組んでおります。</p>
</dd>

</dl>
<dl class="cf">
<dt class="pc"><img src="<?php bloginfo('template_url'); ?>/images/business_photo2.jpg" alt="レンタル事業"></dt>
<dd><h4>レンタル事業</h4>
<p class="sp"><img src="<?php bloginfo('template_url'); ?>/images/business_photo2.jpg" alt="レンタル事業"></p>
  <p>レンタル事業は、レストランや工場などのユニフォームを扱うユニフォームレンタル部門とホテルの客室リネン(シーツ、タオル等)やレストランのテーブルクロスなどを扱うリネンサプライ部門で構成されています。<br>
  いずれの部門もお客さま向けの商品を白洋舍で用意し、ご使用の都度クリーニングをして再度お貸出しするというサービスで、「クリーニングの白洋舍」としてのノウハウが生かされます。商品選定のお手伝い、在庫管理や補修、交換などのメンテナンスも請け負いますので、導入時の商品買入のイニシャルコスト削減に加え、管理コストの削減にもつながります。<br>
  ユニフォームレンタルではICチップの埋め込みによるきめ細かな管理やISO22000取得(UR横浜工場)のノウハウによる徹底した衛生管理を実現、高い衛生基準を持つ食品工場のお客さまにもご満足をいただいています。</p>
</dd>

</dl>
<dl class="cf">
<dt class="pc"><img src="<?php bloginfo('template_url'); ?>/images/business_photo3.jpg" alt="クリーンサービス事業"></dt>
<dd><h4>クリーンサービス事業</h4>
<p class="sp"><img src="<?php bloginfo('template_url'); ?>/images/business_photo3.jpg" alt="クリーンサービス事業"></p>
  <p>クリーンサービス事業は、店舗やオフィスに清潔と快適さを提供する、レンタルのマット・モップなどのダストコントロール部門と、清掃サービスからビル管理まで、店舗・オフィスのトータルケアを展開するハウスケア部門に分かれます。<br>
  ダストコントロール部門ではお客さまのニーズに合わせ、レディメード、オーダーメードのレンタルマットを取り揃え、レンタルモップも豊富な品揃えです。また、お届けするマット・モップは洗浄して繰り返し利用し、使用できなくなったものもリサイクルして他の用途で利用する地球にやさしい商品です。<br>
  ハウスクリーニングではオフィスやショップから劇場、ホールなどの清掃、ビル外装タイルや照明器具、カーペットなどの洗浄、害虫コントロールなど、あらゆる環境を清潔・安全に保つお手伝いをいたします。</p>
</dd>

</dl>
</div><!-- wrapper -->
</section>
</section>