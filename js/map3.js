// JavaScript Document
  function attachMessage(marker, msg) {
    google.maps.event.addListener(marker, 'click', function(event) {
      new google.maps.InfoWindow({
        content: msg
      }).open(marker.getMap(), marker);
    });
  }
// 位置情報と表示データの組み合わせ
  var data = new Array();//マーカー位置の緯度経度
  data.push({position: new google.maps.LatLng(35.652938, 139.647773), content: 'カメリアキッズ 豪徳寺園'});
 
  var myMap = new google.maps.Map(document.getElementById('map3'), {
    zoom: 17,//地図縮尺
    center: new google.maps.LatLng(35.652938, 139.647773),//地図の中心点
    scrollwheel: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var gmapmark = 'images/map_icon.png';
  var image = new google.maps.MarkerImage( gmapmark , new google.maps.Size(100,59));
  for (i = 0; i < data.length; i++) {
    var myMarker = new google.maps.Marker({
      position: data[i].position,
      map: myMap
    });
    attachMessage(myMarker, data[i].content);
  }