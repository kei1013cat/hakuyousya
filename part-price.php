

    <section class="price_menu pc">
      <div class="wrapper">
        <ul class="cf">
          <li><a href="#a_mens">紳士服／メンズ</a></li>
          <li><a href="#a_woman">婦人服／レディス</a></li>
          <li><a href="#a_kimono">きもの・ファー・レザー</a></li>
          <li><a href="#a_custom">カスタムクリーニング</a></li>
        </ul>
        <ul class="cf">
          <li><a href="#a_curtain">カーテン</a></li>
          <li><a href="#a_carpet">じゅうたん</a></li>
          <li><a href="#a_futon">ふとん</a></li>
          <li><a href="#a_household">その他の家庭用品</a></li>
        </ul>
      </div>
    </section>

    <section class="price">
      <div class="wrapper">
        <section id="a_mens">
          <h2 class="headline02"><span>紳士服／メンズ</span></h2>

          <?php if(is_pc()): ?>
            <h3 class="headline03">ドライクリーニング</h3>
          <?php else: ?>
            <dl id="acMenu">
            <dt class="cf">ドライクリーニング<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

          <?php if(is_pc()): ?>
            <ul class="tab1 tab">
              <li class="select">店舗持ち込み</li>
              <li>集配サービス</li>
            </ul>
            <ul class="content1 content">
            <li>
          <?php else: ?>
            <dl id="acMenuDetail">
            <dt class="cf">店舗持ち込み<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

              <table cellspacing="0" cellpadding="0">
                <thead>
                  <tr>
                    <th>品名</th>
                    <th>標準料金</th>
                    <th><a class="open-options1" href="#">ローヤル料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint1.php" );?>
                    <th><a class="open-options2" href="#">高級ブランド・<br>素材別料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint2.php" );?>
                    <th><a class="open-options3" href="#">クリスタル料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint3.php" );?>

                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>背広（上　下）</th>
                    <td>1,800円<span class="tax">+税</span></td>
                    <td>3,550円<span class="tax">+税</span></td>
                    <td>3,550円<span class="tax">+税</span></td>
                    <td>3,550円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th >背広（三　揃）</th>
                    <td >2,470円<span class="tax">+税</span></td>
                    <td >4,900円<span class="tax">+税</span></td>
                    <td >4,900円<span class="tax">+税</span></td>
                    <td >4,900円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>背広（上　衣）</th>
                    <td>1,130円<span class="tax">+税</span></td>
                    <td>2,200円<span class="tax">+税</span></td>
                    <td>2,200円<span class="tax">+税</span></td>
                    <td>2,200円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th >背広（チョッキ）</th>
                    <td >670円<span class="tax">+税</span></td>
                    <td >1,350円<span class="tax">+税</span></td>
                    <td >1,350円<span class="tax">+税</span></td>
                    <td >1,350円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ズボン</th>
                    <td>670円<span class="tax">+税</span></td>
                    <td>1,350円<span class="tax">+税</span></td>
                    <td>1,350円<span class="tax">+税</span></td>
                    <td>1,350円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th >コート・トレンチ</th>
                    <td >1,800円<span class="tax">+税</span></td>
                    <td >3,550円<span class="tax">+税</span></td>
                    <td >3,550円<span class="tax">+税</span></td>
                    <td >3,550円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>半コート</th>
                    <td>1,600円<span class="tax">+税</span></td>
                    <td>3,150円<span class="tax">+税</span></td>
                    <td>3,150円<span class="tax">+税</span></td>
                    <td>3,150円<span class="tax">+税</span></td>
                  </tr>
                  <tr >
                    <th>ジャンパー</th>
                    <td>900円<span class="tax">+税</span>～
                      より</td>
                    <td>－</td>
                    <td>1,750円<span class="tax">+税</span>～ より</td>
                    <td>1,750円<span class="tax">+税</span>～ より</td>
                  </tr>
                  <tr>
                    <th>ダウンジャケット</th>
                    <td>2,100円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>4,200円<span class="tax">+税</span></td>
                    <td>－</td>
                  </tr>
                  <tr >
                    <th>スキーズボン</th>
                    <td>1,450円<span class="tax">+税</span>～
                      より</td>
                    <td>－</td>
                    <td>－</td>
                    <td>－</td>
                  </tr>
                  <tr>
                    <th>学生服（中学生以上）上下</th>
                    <td>1,200円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>－</td>
                    <td>－</td>
                  </tr>
                  <tr>
                    <th >ネクタイ</th>
                    <td >450円<span class="tax">+税</span></td>
                    <td >－</td>
                    <td >800円<span class="tax">+税</span></td>
                    <td >－</td>
                  </tr>
                  <tr>
                    <th>セーター・カーディガン・チョッキ</th>
                    <td>600円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>1,150円<span class="tax">+税</span></td>
                    <td>1,150円<span class="tax">+税</span></td>
                  </tr>
                  <tr >
                    <th>〃　手編み・装飾もの</th>
                    <td>1,050円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>2,100円<span class="tax">+税</span></td>
                    <td>2,100円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>〃　バルキー・マキシ・カウチン</th>
                    <td>1,050円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>2,100円<span class="tax">+税</span></td>
                    <td>2,100円<span class="tax">+税</span></td>
                  </tr>
                  <tr >
                    <th>スポーツシャツ</th>
                    <td>600円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>1,150円<span class="tax">+税</span></td>
                    <td>1,150円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>マフラー</th>
                    <td>600円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>1,150円<span class="tax">+税</span></td>
                    <td>－</td>
                  </tr>
                </tbody>
              </table>

          <?php if(is_pc()): ?>
            </li>
            <li class="hide">
          <?php else: ?>
            </dd></dl>
            <dl id="acMenuDetail" class="last">
            <dt class="cf">集配サービス<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

              <table cellspacing="0" cellpadding="0">
                <thead>
                  <tr>
                    <th>品名</th>
                    <th>標準料金</th>
                    <th><a class="open-options1" href="#">ローヤル料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint1.php" );?>
                    <th><a class="open-options2" href="#">高級ブランド・<br>素材別料金</a></th>
                     <?php require( dirname(__FILE__)."/include/hint2.php" );?>
                   <th><a class="open-options3" href="#">クリスタル料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint3.php" );?>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th >背広（上　下）</th>
                    <td>1,950円<span class="tax">+税</span></td>
                    <td>3,850円<span class="tax">+税</span></td>
                    <td>3,850円<span class="tax">+税</span></td>
                    <td>3,850円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th  >背広（三　揃）</th>
                    <td >2,670円<span class="tax">+税</span></td>
                    <td >5,300円<span class="tax">+税</span></td>
                    <td >5,300円<span class="tax">+税</span></td>
                    <td >5,300円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>背広（上　衣）</th>
                    <td>1,230円<span class="tax">+税</span></td>
                    <td>2,400円<span class="tax">+税</span></td>
                    <td>2,400円<span class="tax">+税</span></td>
                    <td>2,400円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th >背広（チョッキ）</th>
                    <td >720円<span class="tax">+税</span></td>
                    <td >1,450円<span class="tax">+税</span></td>
                    <td >1,450円<span class="tax">+税</span></td>
                    <td >1,450円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ズボン</th>
                    <td>720円<span class="tax">+税</span></td>
                    <td>1,450円<span class="tax">+税</span></td>
                    <td>1,450円<span class="tax">+税</span></td>
                    <td>1,450円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th >コート・トレンチ</th>
                    <td >1,900円<span class="tax">+税</span></td>
                    <td >3,850円<span class="tax">+税</span></td>
                    <td >3,850円<span class="tax">+税</span></td>
                    <td >3,850円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>半コート</th>
                    <td>1,700円<span class="tax">+税</span></td>
                    <td>3,350円<span class="tax">+税</span></td>
                    <td>3,350円<span class="tax">+税</span></td>
                    <td>3,350円<span class="tax">+税</span></td>
                  </tr>
                  <tr >
                    <th>ジャンパー</th>
                    <td>950円<span class="tax">+税</span>～ より</td>
                    <td>－</td>
                    <td>1,850円<span class="tax">+税</span>～ より</td>
                    <td>1,850円<span class="tax">+税</span>～ より</td>
                  </tr>
                  <tr>
                    <th>ダウンジャケット</th>
                    <td>2,300円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>4,500円<span class="tax">+税</span></td>
                    <td>－</td>
                  </tr>
                  <tr >
                    <th>スキーズボン</th>
                    <td>1,500円<span class="tax">+税</span>～ より</td>
                    <td>－</td>
                    <td>－</td>
                    <td>－</td>
                  </tr>
                  <tr>
                    <th>学生服（中学生以上）上下</th>
                    <td>1,300円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>－</td>
                    <td>－</td>
                  </tr>
                  <tr>
                    <th >ネクタイ</th>
                    <td >500円<span class="tax">+税</span></td>
                    <td >－</td>
                    <td >850円<span class="tax">+税</span></td>
                    <td >－</td>
                  </tr>
                  <tr>
                    <th>セーター・カーディガン・チョッキ</th>
                    <td>650円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>1,250円<span class="tax">+税</span></td>
                    <td>1,250円<span class="tax">+税</span></td>
                  </tr>
                  <tr >
                    <th>〃手編み・装飾もの</th>
                    <td>1,150円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>2,300円<span class="tax">+税</span></td>
                    <td>2,300円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>〃　バルキー・マキシ・カウチン</th>
                    <td>1,150円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>2,300円<span class="tax">+税</span></td>
                    <td>2,300円<span class="tax">+税</span></td>
                  </tr>
                  <tr >
                    <th>スポーツシャツ</th>
                    <td>650円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>1,250円<span class="tax">+税</span></td>
                    <td>1,250円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>マフラー</th>
                    <td>650円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>1,250円<span class="tax">+税</span></td>
                    <td>－</td>
                  </tr>
              </tbody>

          </table>

          <?php if(is_pc()): ?>
            </li>
            </ul>
          <?php else: ?>
            </dd>
            </dl>
          <?php endif; ?>
          <?php if(is_mobile()): ?>
          </dd></dl>
          <?php endif; ?>

          <?php if(is_pc()): ?>
            <h3 class="headline03">ランドリー（水洗い）</h3>
          <?php else: ?>
            <dl id="acMenu">
            <dt class="cf">ランドリー（水洗い）<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

          <?php if(is_pc()): ?>
            <ul class="tab2 tab">
              <li class="select">店舗持ち込み</li>
              <li>集配サービス</li>
            </ul>
            <ul class="content2 content">
            <li>
          <?php else: ?>
           <dl id="acMenuDetail">
            <dt class="cf">店舗持ち込み<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>


              <table cellpadding="0" cellspacing="0">
                <thead>
                  <tr>
                    <th>品名</th>
                    <th>標準料金</th>
                    <th><a class="open-options1" href="#">ローヤル料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint1.php" );?>
                    <th><a class="open-options2" href="#">高級ブランド・<br>素材別料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint2.php" );?>
                    <th><a class="open-options3" href="#">クリスタル料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint3.php" );?>

                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>ワイシャツ</th>
                    <td>290円<span class="tax">+税</span></td>
                    <td>520円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>－</td>
                  </tr>
                  <tr>
                    <th >ワイシャツ（麻・ハイカラー）</th>
                    <td >－</td>
                    <td >600円<span class="tax">+税</span></td>
                    <td >－</td>
                    <td >－</td>
                  </tr>
                  <tr>
                    <th>ワイシャツ（前ヒダ・大胸）</th>
                    <td>－</td>
                    <td>1,400円<span class="tax">+税</span></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <th>Ｔシャツ・トレーナー</th>
                    <td >600円<span class="tax">+税</span></td>
                    <td >－</td>
                    <td >1,150円<span class="tax">+税</span></td>
                    <td >1,150円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>白衣（短）</th>
                    <td>600円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>－</td>
                    <td>－</td>
                  </tr>
                  <tr >
                    <th>白衣（長）</th>
                    <td>650円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
          <?php if(is_pc()): ?>
            </li>
            <li class="hide">
          <?php else: ?>
            </dd></dl>
            <dl id="acMenuDetail" class="last">
            <dt class="cf">集配サービス<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>
              <table cellpadding="3" cellspacing="1">
                <thead>
                  <tr>
                    <th>品名</th>
                    <th>標準料金</th>
                    <th><a class="open-options1" href="#">ローヤル料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint1.php" );?>
                    <th><a class="open-options2" href="#">高級ブランド・<br>素材別料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint2.php" );?>
                    <th><a class="open-options3" href="#">クリスタル料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint3.php" );?>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>ワイシャツ</th>
                    <td>310円<span class="tax">+税</span></td>
                    <td>550円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>－</td>
                  </tr>
                  <tr>
                    <th >ワイシャツ（麻・ハイカラー）</th>
                    <td >－</td>
                    <td >630円<span class="tax">+税</span></td>
                    <td >－</td>
                    <td >－</td>
                  </tr>
                  <tr>
                    <th>ワイシャツ（前ヒダ・大胸）</th>
                    <td>－</td>
                    <td>1500円<span class="tax">+税</span></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <th>Ｔシャツ・トレーナー</th>
                    <td >650円<span class="tax">+税</span></td>
                    <td >－</td>
                    <td >－</td>
                    <td >－</td>
                  </tr>
                  <tr>
                    <th>白衣（短）</th>
                    <td>650円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>－</td>
                    <td>－</td>
                  </tr>
                  <tr >
                    <th>白衣（長）</th>
                    <td>700円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
          <?php if(is_pc()): ?>
            </li>
            </ul>
          <?php else: ?>
            </dd>
            </dl>
          <?php endif; ?>
        </section>



        <h2 class="headline02" id="a_woman"><span>婦人服／レディス</span></h2>
        <section>

          <?php if(is_pc()): ?>
            <h3 class="headline03">ドライクリーニング</h3>
          <?php else: ?>
            <dl id="acMenu">
            <dt class="cf">ドライクリーニング<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

          <?php if(is_pc()): ?>
            <ul class="tab3 tab">
              <li class="select">店舗持ち込み</li>
              <li>集配サービス</li>
            </ul>
            <ul class="content3 content">
            <li>
          <?php else: ?>
           <dl id="acMenuDetail">
            <dt class="cf">店舗持ち込み<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

              <table cellspacing="0" cellpadding="0">
                <thead>
                  <tr>
                    <th>品名</th>
                    <th>標準料金</th>
                    <th><a class="open-options1" href="#">ローヤル料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint1.php" );?>
                    <th><a class="open-options2" href="#">高級ブランド・<br>素材別料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint2.php" );?>
                    <th><a class="open-options3" href="#">クリスタル料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint3.php" );?>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>婦人上衣（半袖含む）</th>
                    <td>950円<span class="tax">+税</span>～より</td>
                    <td>1,900円<span class="tax">+税</span>～より</td>
                    <td>1,900円<span class="tax">+税</span>～より</td>
                    <td>1,900円<span class="tax">+税</span>～より</td>
                  </tr>
                  <tr>
                    <th>婦人上衣（ベスト）</th>
                    <td>550円<span class="tax">+税</span></td>
                    <td>1,100円<span class="tax">+税</span></td>
                    <td>1,100円<span class="tax">+税</span></td>
                    <td>1,100円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>スカート（ミニ・タイト）</th>
                    <td>600円<span class="tax">+税</span></td>
                    <td>1,150円<span class="tax">+税</span></td>
                    <td>1,150円<span class="tax">+税</span></td>
                    <td>1,150円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>スカート（フレア・ヒダ3本まで）</th>
                    <td>600円<span class="tax">+税</span></td>
                    <td>1,150円<span class="tax">+税</span></td>
                    <td>1,150円<span class="tax">+税</span></td>
                    <td>1,150円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>スカート（ヒダ4-30本まで・マキシ）</th>
                    <td>1,050円<span class="tax">+税</span></td>
                    <td>2,100円<span class="tax">+税</span></td>
                    <td>2,100円<span class="tax">+税</span></td>
                    <td>－</td>
                  </tr>
                  <tr>
                    <th>総プリーツ</th>
                    <td>－</td>
                    <td>2,550円<span class="tax">+税</span></td>
                    <td>2,550円<span class="tax">+税</span></td>
                    <td>－</td>
                  </tr>
                  <tr>
                    <th>スラックス</th>
                    <td>670円<span class="tax">+税</span></td>
                    <td>1,350円<span class="tax">+税</span></td>
                    <td>1,350円<span class="tax">+税</span></td>
                    <td>1,350円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>女子学生服　上下</th>
                    <td>1,250円<span class="tax">+税</span>～より</td>
                    <td>－</td>
                    <td>－</td>
                    <td>－</td>
                  </tr>
                  <tr>
                    <th>ブラウス</th>
                    <td>600円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>1,200円<span class="tax">+税</span></td>
                    <td>1,200円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ブラウス（飾り付き）</th>
                    <td>750円<span class="tax">+税</span></td>
                    <td>1,550円<span class="tax">+税</span></td>
                    <td>1,550円<span class="tax">+税</span></td>
                    <td>1,550円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ワンピース（袖無し・普通品）</th>
                    <td>950円<span class="tax">+税</span></td>
                    <td>1,900円<span class="tax">+税</span></td>
                    <td>1,900円<span class="tax">+税</span></td>
                    <td>1,900円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ワンピース（袖無し・ヒダ11本以上）</th>
                    <td>1,150円<span class="tax">+税</span></td>
                    <td>2,300円<span class="tax">+税</span></td>
                    <td>2,300円<span class="tax">+税</span></td>
                    <td>2,300円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ワンピース（半袖・長袖・普通品）</th>
                    <td>1,300円<span class="tax">+税</span></td>
                    <td>2,650円<span class="tax">+税</span></td>
                    <td>2,650円<span class="tax">+税</span></td>
                    <td>2,650円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ワンピース（ヒダ11本以上）</th>
                    <td>1,600円<span class="tax">+税</span></td>
                    <td>3,150円<span class="tax">+税</span></td>
                    <td>3,150円<span class="tax">+税</span></td>
                    <td>3,150円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ウェディングドレス（本体のみ）</th>
                    <td>－</td>
                    <td>12,000円<span class="tax">+税</span></td>
                    <td>[箱入りは+800円<span class="tax">+税</span>］</td>
                    <td>[箱入りは+800円<span class="tax">+税</span>］</td>
                  </tr>
                  <tr>
                    <th>ウェディングドレス（ベール・ペチコート）</th>
                    <td>－</td>
                    <td>16,000円<span class="tax">+税</span></td>
                    <td>[箱入りは+800円<span class="tax">+税</span>］</td>
                    <td>[箱入りは+800円<span class="tax">+税</span>］</td>
                  </tr>
                  <tr>
                    <th>スカーフ（大）</th>
                    <td>850円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>1,650円<span class="tax">+税</span></td>
                    <td>－</td>
                  </tr>
                  <tr>
                    <th>スカーフ（小 80cm×80cmまで）</th>
                    <td>650円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>1,250円<span class="tax">+税</span></td>
                    <td>－</td>
                  </tr>
                  <tr>
                    <th>ショール</th>
                    <td>850円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>1,650円<span class="tax">+税</span></td>
                    <td>－</td>
                  </tr>
                </tbody>
              </table>
          <?php if(is_pc()): ?>
            </li>
            <li class="hide">
          <?php else: ?>
            </dd></dl>
            <dl id="acMenuDetail" class="last">
            <dt class="cf">集配サービス<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>
              <table cellspacing="0" cellpadding="0">
                <thead>
                  <tr>
                    <th>品名</th>
                    <th>標準料金</th>
                    <th><a class="open-options1" href="#">ローヤル料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint1.php" );?>
                    <th><a class="open-options2" href="#">高級ブランド・<br>素材別料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint2.php" );?>
                    <th><a class="open-options3" href="#">クリスタル料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint3.php" );?>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>婦人上衣（半袖含む）</th>
                    <td>1,000円<span class="tax">+税</span>～より</td>
                    <td>2,000円<span class="tax">+税</span>～より</td>
                    <td>2,000円<span class="tax">+税</span>～より</td>
                    <td>2,000円<span class="tax">+税</span>～より</td>
                  </tr>
                  <tr>
                    <th>婦人上衣（ベスト）</th>
                    <td>600円<span class="tax">+税</span></td>
                    <td>1,200円<span class="tax">+税</span></td>
                    <td>1,200円<span class="tax">+税</span></td>
                    <td>1,200円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>スカート（ミニ・タイト）</th>
                    <td>650円<span class="tax">+税</span></td>
                    <td>1,250円<span class="tax">+税</span></td>
                    <td>1,250円<span class="tax">+税</span></td>
                    <td>1,250円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>スカート（フレア・ヒダ3本まで）</th>
                    <td>650円<span class="tax">+税</span></td>
                    <td>1,250円<span class="tax">+税</span></td>
                    <td>1,250円<span class="tax">+税</span></td>
                    <td>1,250円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>スカート（ヒダ4-30本まで・マキシ）</th>
                    <td>1,150円<span class="tax">+税</span></td>
                    <td>2,300円<span class="tax">+税</span></td>
                    <td>2,300円<span class="tax">+税</span></td>
                    <td>－</td>
                  </tr>
                  <tr>
                    <th>総プリーツ</th>
                    <td>－</td>
                    <td>2,750円<span class="tax">+税</span></td>
                    <td>2,750円<span class="tax">+税</span></td>
                    <td>－</td>
                  </tr>
                  <tr>
                    <th>スラックス</th>
                    <td>720円<span class="tax">+税</span></td>
                    <td>1,450円<span class="tax">+税</span></td>
                    <td>1,450円<span class="tax">+税</span></td>
                    <td>1,450円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>女子学生服　上下</th>
                    <td>1,350円<span class="tax">+税</span>～より</td>
                    <td>－</td>
                    <td>－</td>
                    <td>－</td>
                  </tr>
                  <tr>
                    <th>ブラウス</th>
                    <td>650円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>1,300円<span class="tax">+税</span></td>
                    <td>1,300円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ブラウス（飾り付き）</th>
                    <td>800円<span class="tax">+税</span></td>
                    <td>1,650円<span class="tax">+税</span></td>
                    <td>1,650円<span class="tax">+税</span></td>
                    <td>1,650円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ワンピース（袖無し・普通品）</th>
                    <td>1,000円<span class="tax">+税</span></td>
                    <td>2,000円<span class="tax">+税</span></td>
                    <td>2,000円<span class="tax">+税</span></td>
                    <td>2,000円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ワンピース（袖無し・ヒダ11本以上）</th>
                    <td>1,250円<span class="tax">+税</span></td>
                    <td>2,550円<span class="tax">+税</span></td>
                    <td>2,550円<span class="tax">+税</span></td>
                    <td>2,550円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ワンピース（半袖・長袖・普通品）</th>
                    <td>1,400円<span class="tax">+税</span></td>
                    <td>2,850円<span class="tax">+税</span></td>
                    <td>2,850円<span class="tax">+税</span></td>
                    <td>2,850円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ワンピース（ヒダ11本以上）</th>
                    <td>1,700円<span class="tax">+税</span></td>
                    <td>3,350円<span class="tax">+税</span></td>
                    <td>3,350円<span class="tax">+税</span></td>
                    <td>3,350円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ウェディングドレス（本体のみ）</th>
                    <td>－</td>
                    <td>13,200円<span class="tax">+税</span></td>
                    <td>[箱入りは+800円<span class="tax">+税</span>］</td>
                    <td>[箱入りは+800円<span class="tax">+税</span>］</td>
                  </tr>
                  <tr>
                    <th>ウェディングドレス（ベール・ペチコート）</th>
                    <td>－</td>
                    <td>17,600円<span class="tax">+税</span></td>
                    <td>[箱入りは+800円<span class="tax">+税</span>］</td>
                    <td>[箱入りは+800円<span class="tax">+税</span>］</td>
                  </tr>
                  <tr>
                    <th>スカーフ（大）</th>
                    <td>900円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>1,750円<span class="tax">+税</span></td>
                    <td>－</td>
                  </tr>
                  <tr>
                    <th>スカーフ（小 80cm×80cmまで）</th>
                    <td>700円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>1,400円<span class="tax">+税</span></td>
                    <td>－</td>
                  </tr>
                  <tr>
                    <th>ショール</th>
                    <td>900円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>1,750円<span class="tax">+税</span></td>
                    <td>－</td>
                  </tr>
                </tbody>
              </table>
          <?php if(is_pc()): ?>
            </li>
            </ul>
          <?php else: ?>
            </dd>
            </dl>
          <?php endif; ?>

          <?php if(is_mobile    ()): ?>
          </dd></dl>
          <?php endif; ?>
      </section>

       <section>

          <?php if(is_pc()): ?>
            <h3 class="headline03">ランドリー（水洗い）</h3>
          <?php else: ?>
            <dl id="acMenu">
            <dt class="cf">ランドリー（水洗い）<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>



          <?php if(is_pc()): ?>
            <ul class="tab16 tab">
              <li class="select">店舗持ち込み</li>
              <li>集配サービス</li>
            </ul>
            <ul class="content16 content">
            <li>
          <?php else: ?>
            <dl id="acMenuDetail">
            <dt class="cf">店舗持ち込み<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

               <table cellspacing="0" cellpadding="0">
                <thead>
                  <tr>
                    <th>品名</th>
                    <th>標準料金</th>
                    <th><a class="open-options1" href="#">ローヤル料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint1.php" );?>
                    <th><a class="open-options2" href="#">高級ブランド・<br>素材別料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint2.php" );?>
                    <th><a class="open-options3" href="#">クリスタル料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint3.php" );?>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>婦人ワイシャツ</th>
                    <td>290円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>－</td>
                    <td>－</td>
                  </tr>
                  <tr>
                    <th>ゆかた（ねまき用）</th>
                    <td>800円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>－</td>
                    <td>－</td>
                  </tr>
                  <tr>
                    <th>ゆかた（外出用）</th>
                    <td>2,500円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>－</td>
                    <td>－</td>
                  </tr>
                </tbody>
              </table>


          <?php if(is_pc()): ?>
            </li>
            <li class="hide">
          <?php else: ?>
            </dd></dl>
            <dl id="acMenuDetail" class="last">
            <dt class="cf">集配サービス<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>
              <table cellspacing="0" cellpadding="0">
                <thead>
                  <tr>
                    <th>品名</th>
                    <th>標準料金</th>
                    <th><a class="open-options1" href="#">ローヤル料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint1.php" );?>
                    <th><a class="open-options2" href="#">高級ブランド・<br>素材別料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint2.php" );?>
                    <th><a class="open-options3" href="#">クリスタル料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint3.php" );?>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>婦人ワイシャツ</th>
                    <td>310円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>－</td>
                    <td>－</td>
                  </tr>
                  <tr>
                    <th>ゆかた（ねまき用）</th>
                    <td>850円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>－</td>
                    <td>－</td>
                  </tr>
                  <tr>
                    <th>ゆかた（外出用）</th>
                    <td>2,700円<span class="tax">+税</span></td>
                    <td>－</td>
                    <td>－</td>
                    <td>－</td>
                  </tr>
                </tbody>
              </table>
         <?php if(is_pc()): ?>
            </li>
            </ul>
          <?php else: ?>
            </dd>
            </dl>
          <?php endif; ?>

          <?php if(is_mobile()): ?>
          </dd></dl>
          <?php endif; ?>

        </section>
        <section id="a_kakou">

          <?php if(is_pc()): ?>
            <h3 class="headline03">各種加工</h3>
          <?php else: ?>
            <dl id="acMenu">
            <dt class="cf">各種加工<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

          <?php if(is_pc()): ?>
            <ul class="tab4 tab">
              <li class="select">店舗持ち込み / 集配サービス</li>
            </ul>
            <ul class="content4 content">
            <li>
          <?php endif; ?>

              <?php if(is_pc()): ?>
              <table cellspacing="0" cellpadding="0" class="col2">
              <?php else: ?>
              <h4 class="msg">店舗持ち込み / 集配サービス</h4>
              <table cellspacing="0" cellpadding="0" class="tab col2">
              <?php endif; ?>
                <thead>
                  <tr>
                    <th>品名</th>
                    <th>標準料金</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>シャキッと加工</th>
                    <td>標準料金の50％増しとなります<br> 但し、最低　300円<span class="tax">+税</span><br> 上限2,200円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>はっ水加工</th>
                    <td>標準料金の50％増しとなります<br> 但し、最低　300円<span class="tax">+税</span><br> 上限2,200円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>防虫加工</th>
                    <td>標準料金の50％増しとなります<br> 但し、最低　300円<span class="tax">+税</span><br> 上限2,200円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>汗すっきり加工</th>
                    <td>標準料金の50％増しとなります<br> 但し、最低　300円<span class="tax">+税</span><br> 上限2,200円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>抗菌防臭加工</th>
                    <td>標準料金の50％増しとなります<br> 但し、最低　300円<span class="tax">+税</span><br> 上限1,500円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>折目加工</th>
                    <td>※ズボン・スラックス　1,000円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>防炎加工</th>
                    <td>標準料金の50％増しとなります</td>
                  </tr>
                </tbody>
              </table>

          <?php if(is_pc()): ?>
            </li>
            </ul>
          <?php endif; ?>

          <?php if(is_mobile()): ?>
          </dd></dl>
          <?php endif; ?>

        </section>

        <h2 class="headline02" id="a_kimono"><span>きもの・ファー・レザー</span></h2>
        <section>

          <?php if(is_pc()): ?>
            <h3 class="headline03">きもの和洗＜やわらぎ＞</h3>
          <?php else: ?>
            <dl id="acMenu">
            <dt class="cf">きもの和洗＜やわらぎ＞<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

          <?php if(is_pc()): ?>
            <ul class="tab5 tab">
              <li class="select">店舗持ち込み</li>
              <li>集配サービス</li>
            </ul>
            <ul class="content5 content">
            <li>
          <?php else: ?>
            <dl id="acMenuDetail">
            <dt class="cf">店舗持ち込み<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>
              <table cellspacing="0" cellpadding="0" class="col2">
                <thead>
                  <tr>
                    <th>品名</th>
                    <th>標準料金</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>訪問着・つけさげ</th>
                    <td>9,500円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ひよく付き留袖</th>
                    <td>11,000円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>きもの　袷</th>
                    <td>9,000円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>〃　　　単</th>
                    <td>8,000円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>長じゅばん</th>
                    <td>4,000円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>〃　　　化繊</th>
                    <td>2,500円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>名古屋おび</th>
                    <td>4,500円<span class="tax">+税</span></td>
                  </tr>
                </tbody>
              </table>

          <?php if(is_pc()): ?>
            </li>
            <li class="hide">
          <?php else: ?>
            </dd></dl>
            <dl id="acMenuDetail" class="last">
            <dt class="cf">集配サービス<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

              <table cellspacing="0" cellpadding="0" class="col2">
                <thead>
                  <tr>
                    <th>品名</th>
                    <th>標準料金</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>訪問着・つけさげ</th>
                    <td>10,400円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ひよく付き留袖</th>
                    <td>12,100円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>きもの　袷</th>
                    <td>9,900円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>〃　　　単</th>
                    <td>8,800円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>長じゅばん</th>
                    <td>4,400円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>〃　　　化繊</th>
                    <td>2,700円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>名古屋おび</th>
                    <td>4,900円<span class="tax">+税</span></td>
                  </tr>
                </tbody>
              </table>

          <?php if(is_pc()): ?>
            </li>
            </ul>
          <?php else: ?>
            </dd>
            </dl>
          <?php endif; ?>

          <?php if(is_mobile()): ?>
          </dd></dl>
          <?php endif; ?>

        </section>
        <section id="a_fur">

          <?php if(is_pc()): ?>
            <h3 class="headline03">ファー（毛皮）</h3>
          <?php else: ?>
            <dl id="acMenu">
            <dt class="cf">ファー（毛皮）<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

          <?php if(is_pc()): ?>
            <ul class="tab6 tab">
              <li class="select">店舗持ち込み</li>
              <li>集配サービス</li>
            </ul>
            <ul class="content6 content">
            <li>
          <?php else: ?>
            <dl id="acMenuDetail">
            <dt class="cf">店舗持ち込み<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

              <table cellspacing="0" cellpadding="0" class="col2">
                <thead>
                  <tr>
                    <th>品名 </th>
                    <th>標準料金</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>毛皮コート</th>
                    <td>8,500円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>毛皮半コート</th>
                    <td>7,500円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>毛皮ジャケット</th>
                    <td>5,000円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>カラー</th>
                    <td>2,000円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>敷物ムートン一匹物</th>
                    <td>3,500円<span class="tax">+税</span>～</td>
                  </tr>
                </tbody>
              </table>

          <?php if(is_pc()): ?>
            </li>
            <li class="hide">
          <?php else: ?>
            </dd></dl>
            <dl id="acMenuDetail" class="last">
            <dt class="cf">集配サービス<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

              <table cellspacing="0" cellpadding="0" class="col2">
                <thead>
                  <tr>
                    <th>品名 </th>
                    <th>標準料金</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>毛皮コート</th>
                    <td>9,300円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>毛皮半コート</th>
                    <td>8,200円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>毛皮ジャケット</th>
                    <td>5,500円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>カラー</th>
                    <td>2,200円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>敷物ムートン一匹物</th>
                    <td>3,800円<span class="tax">+税</span>～</td>
                  </tr>
                </tbody>
              </table>
          <?php if(is_pc()): ?>
            </li>
            </ul>
          <?php else: ?>
            </dd>
            </dl>
          <?php endif; ?>

          <?php if(is_mobile()): ?>
          </dd></dl>
          <?php endif; ?>
        </section>

        <section id="a_leather">


          <?php if(is_pc()): ?>
            <h3 class="headline03">レザー（皮革）</h3>
          <?php else: ?>
            <dl id="acMenu">
            <dt class="cf">レザー（皮革）<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

          <?php if(is_pc()): ?>
            <ul class="tab7 tab">
              <li class="select">店舗持ち込み</li>
              <li>集配サービス</li>
            </ul>
            <ul class="content7 content">
            <li>
          <?php else: ?>
            <dl id="acMenuDetail">
            <dt class="cf">店舗持ち込み<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>
              <table cellspacing="0" cellpadding="0" class="col2">
                <thead>
                  <tr>
                    <th>品名 </th>
                    <th>標準料金</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>革コート</th>
                    <td>8,000円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>革半コート</th>
                    <td>7,000円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>革上着・ジャンパー</th>
                    <td>5,500円<span class="tax">+税</span></td>
                  </tr>
                </tbody>
              </table>
          <?php if(is_pc()): ?>
            </li>
            <li class="hide">
          <?php else: ?>
            </dd></dl>
            <dl id="acMenuDetail" class="last">
            <dt class="cf">集配サービス<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>
              <table cellspacing="0" cellpadding="0" class="col2">
                <thead>
                  <tr>
                    <th>品名 </th>
                    <th>標準料金</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>革コート</th>
                    <td>8,800円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>革半コート</th>
                    <td>7,700円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>革上着・ジャンパー</th>
                    <td>6,000円<span class="tax">+税</span></td>
                  </tr>
                </tbody>
              </table>
          <?php if(is_pc()): ?>
            </li>
            </ul>
          <?php else: ?>
            </dd>
            </dl>
          <?php endif; ?>

          <?php if(is_mobile    ()): ?>
          </dd></dl>
          <?php endif; ?>

        </section>

        <h2 class="headline02" id="a_custom"><span>カスタムクリーニング</span></h2>
        <section>

          <?php if(is_pc()): ?>
            <h3 class="headline03">カスタムクリーニング</h3>
          <?php else: ?>
            <dl id="acMenu">
            <dt class="cf">カスタムクリーニング<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

          <?php if(is_pc()): ?>
          <ul class="tab8 tab">
            <li class="select">店舗持ち込み / 集配サービス</li>
          </ul>
          <ul class="content8 content">
            <li>
          <?php endif; ?>
              <?php if(is_pc()): ?>
                  <table cellspacing="0" cellpadding="0" class="col2">
              <?php else: ?>
                  <h4 class="msg">店舗持ち込み / 集配サービス</h4>
                  <table cellspacing="0" cellpadding="0" class="tab col2">
              <?php endif; ?>
                <thead>
                  <tr>
                    <th>品名 </th>
                    <th>標準料金</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>背広（上下）</th>
                    <td>7,000円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>背広（三揃）</th>
                    <td>9,000円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>背広（ベスト）</th>
                    <td>2,500円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ジャケット</th>
                    <td>4,500円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>スラックス</th>
                    <td>3,000円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>礼服（上下）</th>
                    <td>9,000円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>礼服（三揃）</th>
                    <td>11,500円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>コート</th>
                    <td>7,000円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>半コート</th>
                    <td>5,500円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ジャンパー</th>
                    <td>4,500円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>セーター</th>
                    <td>2,500円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>ニットアンサンブル</th>
                    <td>5,000円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ブラウス</th>
                    <td>2,500円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>スポーツシャツ</th>
                    <td>3,000円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>婦人（上下）</th>
                    <td>6,000円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>婦人（上衣）</th>
                    <td>4,000円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>スカート</th>
                    <td>3,000円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>婦人スラックス</th>
                    <td>3,000円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ワンピース</th>
                    <td>4,000円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>ドレス</th>
                    <td>8,500円<span class="tax">+税</span>～</td>
                  </tr>
                </tbody>
              </table>

          <?php if(is_pc()): ?>
            </li>
          </ul>
         <?php endif; ?>

          <?php if(is_mobile    ()): ?>
          </dd></dl>
          <?php endif; ?>
        </section>

        <h2 class="headline02" id="a_curtain"><span>カーテン</span></h2>
        <section>
          <?php if(is_mobile()): ?>
            <dl id="acMenu">
            <dt class="cf">カーテン<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

          <?php if(is_pc()): ?>
            <ul class="tab9 tab">
              <li class="select">店舗持ち込み</li>
              <li>集配サービス</li>
            </ul>
            <ul class="content9 content">
            <li>
          <?php else: ?>
            <dl id="acMenuDetail">
            <dt class="cf">店舗持ち込み<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

              <table cellspacing="0" cellpadding="0" class="col2">
                <thead>
                  <tr>
                    <th>品名</th>
                    <th>標準料金</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>ドレープ（裏付）m2</th>
                    <td>600円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>ドレープ（裏なし）m2</th>
                    <td>450円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>レース・ケースメントm2</th>
                    <td>450円<span class="tax">+税</span>～</td>
                  </tr>
                </tbody>
              </table>
          <?php if(is_pc()): ?>
            </li>
            <li class="hide">
          <?php else: ?>
            </dd></dl>
            <dl id="acMenuDetail" class="last">
            <dt class="cf">集配サービス<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>
              <table cellspacing="0" cellpadding="0" class="col2">
                <thead>
                  <tr>
                    <th>品名</th>
                    <th>標準料金</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>ドレープ（裏付）m2</th>
                    <td>650円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>ドレープ（裏なし）m2</th>
                    <td>500円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>レース・ケースメントm2</th>
                    <td>500円<span class="tax">+税</span>～</td>
                  </tr>
                </tbody>
              </table>
          <?php if(is_pc()): ?>
            </li>
            </ul>
          <?php else: ?>
            </dd>
            </dl>
          <?php endif; ?>

          <?php if(is_mobile()): ?>
          </dd></dl>
          <?php endif; ?>

        </section>

        <h2 class="headline02" id="a_carpet"><span>じゅうたん</span></h2>
        <section>
          <?php if(is_pc()): ?>
            <h3 class="headline03">一般クリーニング</h3>
          <?php else: ?>
            <dl id="acMenu">
            <dt class="cf">一般クリーニング<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

          <?php if(is_pc()): ?>
            <ul class="tab10 tab">
              <li class="select">店舗持ち込み</li>
              <li>集配サービス</li>
            </ul>
            <ul class="content10 content">
            <li>
          <?php else: ?>
            <dl id="acMenuDetail">
            <dt class="cf">店舗持ち込み<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

              <table cellspacing="0" cellpadding="0" class="col3">
                <thead>
                  <tr>
                    <th>品名</th>
                    <th>１ｍ２当たり料金</th>
                    <th>１畳当たり料金</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>ウィルトン系</th>
                    <td>790円<span class="tax">+税</span>～</td>
                    <td>1,300円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>中国だんつう（機械織り）</th>
                    <td>2,550円<span class="tax">+税</span>～</td>
                    <td>4,200円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>ペルシャ</th>
                    <td>3,210円<span class="tax">+税</span>～</td>
                    <td>5,300円<span class="tax">+税</span>～</td>
                  </tr>
                </tbody>
              </table>

          <?php if(is_pc()): ?>
            </li>
            <li class="hide">
          <?php else: ?>
            </dd></dl>
            <dl id="acMenuDetail" class="last">
            <dt class="cf">集配サービス<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

              <table cellspacing="0" cellpadding="0" class="col3">
                <thead>
                  <tr>
                    <th>品名</th>
                    <th>１ｍ２当たり料金</th>
                    <th>１畳当たり料金</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>ウィルトン系</th>
                    <td>850円<span class="tax">+税</span>～</td>
                    <td>1,400円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>中国だんつう（機械織り）</th>
                    <td>2,790円<span class="tax">+税</span>～</td>
                    <td>4,600円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>ペルシャ</th>
                    <td>3,520円<span class="tax">+税</span>～</td>
                    <td>5,800円<span class="tax">+税</span>～</td>
                  </tr>
                </tbody>
              </table>

          <?php if(is_pc()): ?>
            </li>
            </ul>
          <?php else: ?>
            </dd>
            </dl>
          <?php endif; ?>

          <?php if(is_mobile()): ?>
          </dd></dl>
          <?php endif; ?>

        </section>

        <section>
          <?php if(is_pc()): ?>
            <h3 class="headline03">特注クリーニング</h3>
          <?php else: ?>
            <dl id="acMenu">
            <dt class="cf">特注クリーニング<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

          <?php if(is_pc()): ?>
            <ul class="tab11 tab">
              <li class="select">店舗持ち込み</li>
              <li>集配サービス</li>
            </ul>
            <ul class="content11 content">
            <li>
          <?php else: ?>
            <dl id="acMenuDetail">
            <dt class="cf">店舗持ち込み<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>
              <table cellspacing="0" cellpadding="0" class="col3">
                <thead>
                  <tr>
                    <th>品名</th>
                    <th>１ｍ２当たり料金</th>
                    <th>１畳当たり料金</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>中国だんつう（機械織り）</th>
                    <td>5,000円<span class="tax">+税</span>～</td>
                    <td>8,250円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>中国だんつう（手織り）</th>
                    <td>5,760円<span class="tax">+税</span>～</td>
                    <td>9,500円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>ペルシャだんつう（手織り）</th>
                    <td>8,790円<span class="tax">+税</span>～</td>
                    <td>14,500円<span class="tax">+税</span>～</td>
                  </tr>
                </tbody>
              </table>
          <?php if(is_pc()): ?>
            </li>
            <li class="hide">
          <?php else: ?>
            </dd></dl>
            <dl id="acMenuDetail" class="last">
            <dt class="cf">集配サービス<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>
              <table cellspacing="0" cellpadding="0" class="col3">
                <thead>
                  <tr>
                    <th>品名</th>
                    <th>１ｍ２当たり料金</th>
                    <th>１畳当たり料金</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>中国だんつう（機械織り）</th>
                    <td>5,300円<span class="tax">+税</span>～</td>
                    <td>8,750円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>中国だんつう（手織り）</th>
                    <td>6,060円<span class="tax">+税</span>～</td>
                    <td>10,000円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>ペルシャだんつう（手織り）</th>
                    <td>9,390円<span class="tax">+税</span>～</td>
                    <td>15,500円<span class="tax">+税</span>～</td>
                  </tr>
                </tbody>
              </table>
          <?php if(is_pc()): ?>
            </li>
            </ul>
          <?php else: ?>
            </dd>
            </dl>
          <?php endif; ?>

          <?php if(is_mobile()): ?>
          </dd></dl>
          <?php endif; ?>

        </section>

        <section>
          <?php if(is_pc()): ?>
            <h3 class="headline03">メンテナンスクリーニング</h3>
          <?php else: ?>
            <dl id="acMenu">
            <dt class="cf">メンテナンスクリーニング<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

          <?php if(is_pc()): ?>
          <table cellspacing="0" cellpadding="0" class="col3">
          <?php else: ?>
          <h4 class="msg">店舗持ち込み / 集配サービス</h4>
          <table cellspacing="0" cellpadding="0" class="tab col3">
          <?php endif; ?>
            <thead>
              <tr>
                <th colspan="2">品名</th>
                <th>１ｍ２当たり料金</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th rowspan="3">特Aクラス<br> (日数：30日～60日)</th>
                <th>トルコヘレケシルク</th>
                <td rowspan="3">30,000円<span class="tax">+税</span>～</td>
              </tr>
              <tr>
                <th>工房サイン入りペルシャ絨毯</th>
              </tr>
              <tr>
                <th>中国高だん数絨毯300段以上クラス</th>
              </tr>
              <tr>
                <th rowspan="4">Aクラス<br> （日数：20日～40日）</th>
                <th>カシミールシルク</th>
                <td rowspan="4">20,000円<span class="tax">+税</span>～</td>
              </tr>
              <tr>
                <th>ペルシャ絨毯シルク</th>
              </tr>
              <tr>
                <th>ペルシャ絨毯ウール</th>
              </tr>
              <tr>
                <th>シルクロード絨毯等</th>
              </tr>
              <tr>
                <th rowspan="4">オリエンタルクラス<br> （日数：10日～30日）</th>
                <th>パキスタンシルクウール混合</th>
                <td rowspan="4">10,000円<span class="tax">+税</span>～</td>
              </tr>
            </tbody>
          </table>

          <?php if(is_mobile()): ?>
          </dd></dl>
          <?php endif; ?>

        </section>

        <h2 class="headline02" id="a_futon"><span>ふとん</span></h2>

        <section>
          <?php if(is_pc()): ?>
            <h3 class="headline03">ふとん</h3>
          <?php else: ?>
            <dl id="acMenu">
            <dt class="cf">ふとん<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

          <?php if(is_pc()): ?>
            <ul class="tab12 tab">
              <li class="select">店舗持ち込み</li>
              <li>集配サービス</li>
            </ul>
            <ul class="content12 content">
            <li>
          <?php else: ?>
            <dl id="acMenuDetail">
            <dt class="cf">店舗持ち込み<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>
              <table cellspacing="0" cellpadding="0" class="col2">
                <thead>
                  <tr>
                    <th>品名</th>
                    <th>標準料金</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>羽毛布団（掛）S</th>
                    <td>6,000円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>羽毛布団（掛）W</th>
                    <td>8,000円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>羽毛布団（敷）S</th>
                    <td>7,000円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>羽毛布団（敷）W</th>
                    <td>9,000円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>綿・化繊綿布団（掛・敷）Ｓ</th>
                    <td>4,000円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>綿・化繊綿布団（掛・敷）W</th>
                    <td>5,000円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>こたつ布団（綿・化繊）掛・敷</th>
                    <td>3,000円<span class="tax">+税</span></td>
                  </tr>
                </tbody>
              </table>
          <?php if(is_pc()): ?>
            </li>
            <li class="hide">
          <?php else: ?>
            </dd></dl>
            <dl id="acMenuDetail" class="last">
            <dt class="cf">集配サービス<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>
              <table cellspacing="0" cellpadding="0" class="col2">
                <thead>
                  <tr>
                    <th>品名</th>
                    <th>標準料金</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>羽毛布団（掛）S</th>
                    <td>6,500円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>羽毛布団（掛）W</th>
                    <td>8,500円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>羽毛布団（敷）S</th>
                    <td>7,500円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>羽毛布団（敷）W</th>
                    <td>9,500円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>綿・化繊綿布団（掛・敷）Ｓ</th>
                    <td>4,400円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>綿・化繊綿布団（掛・敷）W</th>
                    <td>5,500円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>こたつ布団（綿・化繊）掛・敷</th>
                    <td>3,300円<span class="tax">+税</span></td>
                  </tr>
                </tbody>
              </table>
          <?php if(is_pc()): ?>
            </li>
            </ul>
          <?php else: ?>
            </dd>
            </dl>
          <?php endif; ?>

          <?php if(is_mobile()): ?>
          </dd></dl>
          <?php endif; ?>
        </section>

        <h2 class="headline02" id="a_household"><span>家庭用品</span></h2>
        <section>

          <?php if(is_pc()): ?>
            <h3 class="headline03">ドライクリーニング</h3>
          <?php else: ?>
            <dl id="acMenu">
            <dt class="cf">ドライクリーニング<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

          <?php if(is_pc()): ?>
            <ul class="tab13 tab">
              <li class="select">店舗持ち込み</li>
              <li>集配サービス</li>
            </ul>
            <ul class="content13 content">
            <li>
          <?php else: ?>
            <dl id="acMenuDetail">
            <dt class="cf">店舗持ち込み<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

              <table cellpadding="0" cellspacing="0" class="col3">
                <thead>
                  <tr>
                    <th>品名</th>
                    <th>標準料金</th>
                    <th><a class="open-options2" href="#">高級ブランド・<br>素材別料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint2.php" );?>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>毛布（S・SW）<br> S:140×200 SW:160×200</th>
                    <td>1,100円<span class="tax">+税</span></td>
                    <td>2,100円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ベビー毛布</th>
                    <td>750円<span class="tax">+税</span></td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <th>ひざかけ毛布</th>
                    <td>750円<span class="tax">+税</span></td>
                    <td>1,400円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ベッドスプレッド(S･W薄地)</th>
                    <td>1,200円<span class="tax">+税</span>～より</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <th>ベッドパッド(S)</th>
                    <td>1,100円<span class="tax">+税</span></td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <th>羽根枕</th>
                    <td>1,200円<span class="tax">+税</span></td>
                    <td>&nbsp;</td>
                  </tr>
                </tbody>
              </table>

          <?php if(is_pc()): ?>
            </li>
            <li class="hide">
          <?php else: ?>
            </dd></dl>
            <dl id="acMenuDetail" class="last">
            <dt class="cf">集配サービス<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

              <table cellpadding="0" cellspacing="0" class="col3">
                <thead>
                  <tr>
                    <th >品名</th>
                    <th>標準料金</th>
                    <th><a class="open-options2" href="#">高級ブランド・<br>素材別料金</a></th>
                    <?php require( dirname(__FILE__)."/include/hint2.php" );?>

                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>毛布（S・SW）<br> S:140×200 SW:160×200</th>
                    <td>1,200円<span class="tax">+税</span></td>
                    <td>2,300円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ベビー毛布</th>
                    <td>800円<span class="tax">+税</span></td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <th>ひざかけ毛布</th>
                    <td>800円<span class="tax">+税</span></td>
                    <td>1,500円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ベッドスプレッド(S･W薄地)</th>
                    <td>1,300円<span class="tax">+税</span>～より</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <th>ベッドパッド(S)</th>
                    <td>1,200円<span class="tax">+税</span></td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <th>羽根枕</th>
                    <td>1,300円<span class="tax">+税</span></td>
                    <td>&nbsp;</td>
                  </tr>
                </tbody>
              </table>

          <?php if(is_pc()): ?>
            </li>
            </ul>
          <?php else: ?>
            </dd>
            </dl>
          <?php endif; ?>

          <?php if(is_mobile()): ?>
          </dd></dl>
          <?php endif; ?>
        </section>

        <section>

          <?php if(is_pc()): ?>
            <h3 class="headline03">ランドリー（水洗い）</h3>
          <?php else: ?>
            <dl id="acMenu">
            <dt class="cf">ランドリー（水洗い）<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>

          <?php if(is_pc()): ?>
            <ul class="tab14 tab">
              <li class="select">店舗持ち込み</li>
              <li>集配サービス</li>
            </ul>
            <ul class="content14 content">
            <li>
          <?php else: ?>
            <dl id="acMenuDetail">
            <dt class="cf">店舗持ち込み<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>
              <table cellpadding="0" cellspacing="0" class="col2">
                <thead>
                  <tr>
                    <th>品名</th>
                    <th>標準料金</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>シーツ・フィットシーツ(S･SW･W)</th>
                    <td>550円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>包布(S･SW･W)</th>
                    <td>800円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ピロケース(S･W)</th>
                    <td>400円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>タオルケット(S･SW･W)</th>
                    <td>900円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>テーブルクロス(1ｍ２)</th>
                    <td>500円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>ぬいぐるみ(小)30cmまで</th>
                    <td>1,100円<span class="tax">+税</span>～</td>
                  </tr>
                </tbody>
              </table>
          <?php if(is_pc()): ?>
            </li>
            <li class="hide">
          <?php else: ?>
            </dd></dl>
            <dl id="acMenuDetail" class="last">
            <dt class="cf">集配サービス<p class="accordion_icon"><span></span><span></span></p></dt>
            <dd>
          <?php endif; ?>
              <table cellpadding="0" cellspacing="0" class="col2">
                <thead>
                  <tr>
                    <th>品名</th>
                    <th>標準料金</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>シーツ・フィットシーツ(S･SW･W)</th>
                    <td>600円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>包布(S･SW･W)</th>
                    <td>850円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>ピロケース(S･W)</th>
                    <td>450円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>タオルケット(S･SW･W)</th>
                    <td>950円<span class="tax">+税</span></td>
                  </tr>
                  <tr>
                    <th>テーブルクロス(1ｍ２)</th>
                    <td>550円<span class="tax">+税</span>～</td>
                  </tr>
                  <tr>
                    <th>ぬいぐるみ(小)30cmまで</th>
                    <td>1,200円<span class="tax">+税</span>～</td>
                  </tr>
                </tbody>
              </table>
          <?php if(is_pc()): ?>
            </li>
            </ul>
          <?php else: ?>
            </dd>
            </dl>
          <?php endif; ?>

          <?php if(is_mobile()): ?>
          </dd></dl>
          <?php endif; ?>

        </section>
      </div>
      <!-- wrapper --> 
    </section>
